Materials for the research on GLV and the DCP problem

To play with the jupyter notebook notes.md saved as markdown file, you need jupytext:

`pip install jupytext --upgrade`


```python

```


### Paper outline

1. Introduction of DCP, DCLP and connection to DLP.

     - we want both problems to be difficult

     - key idea: sup(DCP,DLP) = DCLP
2. Proof of hardness of DCLP in the generic group model

   - [ ] prove O(n)? where n is the size of group (TODO, use [1,2,3])

   - claim that DCP is also O(n) due to (1.)
- claim that no Pollard-rho/pohlig-hellman based algorithms are possible
3. Show attacks on DC(L)P and for each why the generic group model does not apply

     - montgomery curve trick

     - bitcoin curve for small bits (Sorina's idea!)

     - show transportations of DCP through isomorphisms
4. Classify a class of (some) polynomials for which the DCP is easy and find a "quick" test for that.

   - [ ] find the class
   - [ ] create and implement the test
   - [ ] apply the tests to all polynomial relevant to side-channel attacks 
5. (no idea how so optional) Statistically show the lack of randomness in the coordinates for the "easy" polynomials compared to the hard polynomials (these cases are "closer" to the generic model).


  [1] https://www.shoup.net/papers/dlbounds1.pdf

  [2] https://math.mit.edu/classes/18.783/2017/LectureNotes10.pdf

  [3] https://crypto.stanford.edu/cs355/19sp/lec11.pdf
