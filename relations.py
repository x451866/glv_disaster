from sage.all import *

def relation_polynomial_weierstrass(l,RX,field):
    X1,X2,X3,a,b = RX.gens()
    semaev3 = RX((X1-X2)**2*X3**2-2*((X1+X2)*(X1*X2+a)+2*b)*X3+(X1*X2-a)**2-4*b*(X1+X2))
    if l==1:
        return semaev3
    FF = PolynomialRing(field,('a','b')).fraction_field()
    a,b = FF.gens()
    E = EllipticCurve(FF,[a,b])
    ml_num = RX(E._multiple_x_numerator(l,X2))
    ml_den = RX(E._multiple_x_denominator(l,X2))
    return (X1-X3)**2*ml_num**2-2*((X1+X3)*(X1*X3+a)+2*b)*ml_num*ml_den+((X1*X3-a)**2-4*b*(X1+X3))*ml_den**2

def relation_polynomial_edwards(RX):
    Y1,Y2,Y3,c,d = RX.gens()
    semaev3 = RX(c*d*(Y1*Y2*Y3)**2-c**3*d*(Y1**2*Y2**2+Y3**2*Y2**2+Y1**2*Y3**2)+2*(c**4*d-1)*Y1*Y2*Y3+(Y1**2+Y2**2+Y3**2)*c-c**3)
    return semaev3

def relation_polynomial_twistededwards(RX):
    Y1,Y2,Y3,a,d = RX.gens()
    semaev3 = Y1**2*Y2**2*Y3**2*d - Y1**2*Y2**2*d - Y1**2*Y3**2*d - Y2**2*Y3**2*d - 2*Y1*Y2*Y3*a + 2*Y1*Y2*Y3*d + Y1**2*a + Y2**2*a + Y3**2*a - a
    return semaev3


def test_relation_polynomial(l=2):
    field = GF(137)
    RX = PolynomialRing(field,['X1','X2','X3','a','b'])
    fr = relation_polynomial_weierstrass(l,RX,field)
    E = EllipticCurve(field,[2,3])
    P = E.random_point()
    Q = E.random_point()
    R = -l*Q-P
    assert fr(X1=P[0],X2=Q[0],X3=R[0],a=2,b=3)==0

def relation_ideal_edwards(poly,l,field, verbose = True):
    """ Find curves for which f(P,Q)=0 means P+lQ+R=0 for some fixed R and small fixed l"""
    """ Take 3rd Semaev polynomial and reduce it modulo given polynomial from Z[x1,x2]. Find x3,a,b so that remainder is zero """
    RX = PolynomialRing(field,['X1','X2','X3','a','b'])
    X1,X2,X3,a,b = RX.gens()
    relation_poly = relation_polynomial(l,RX,field)
    RXQ = RX.quotient(poly)
    X1b,X2b,X3b,ab,bb = RXQ.gens()
    f = RXQ(relation_poly)
    f_decomposition = {} # the dictionary is of the form (deg(X1),deg(X2)):list of [coefficient,deg(X3),deg(a),deg(b)]
    for monomial in f.monomials():
        monlift = monomial.lift()
        coef = f.lift().coefficient(monlift)
        key = (monlift.degrees()[0],monlift.degrees()[1])
        value = [coef,{"degX3":monlift.degrees()[2],"dega":monlift.degrees()[3],"degb":monlift.degrees()[4]}]
        if key in f_decomposition:
            f_decomposition[key].append(value)
        else:
            f_decomposition[key]=[value]
    EquationRing=PolynomialRing(field,['X3','a','b'])
    X3,a,b = EquationRing.gens()
    equations = set()
    for key,value in f_decomposition.items():
        g = 0
        for mon in value:
            coef = ZZ(mon[0].constant_coefficient())
            degrees = mon[1]
            g+=coef*X3**degrees["degX3"]*a**degrees["dega"]*b**degrees["degb"]
        equations.add(EquationRing(g))
    equations = list(equations)
    I = EquationRing.ideal(equations)
    #if I.dimension()==-1:
    #    print("polynomial:",poly)
    #    print("dim:", I.dimension(),", ",I)
    #    print("NO SOLUTION")
    #    print('----')
    #    return
    if I.dimension()==0:
        if verbose:
            print("polynomial:", poly)
            print("dim:", I.dimension(),", ",I)
            print("SOLUTION",I.variety())
            print('----')
        return I.variety()
    if I.dimension()>0:
        if verbose:
            print("polynomial:",poly)
            print("dim:", I.dimension(),", ",I)
            print("MANY SOLUTIONS")
            print('----')
            return [None]
    return []

def relation_ideal_edwards(poly,l,field, verbose = True):
    """ Find curves for which f(P,Q)=0 means P+lQ+R=0 for some fixed R and small fixed l"""
    """ Take 3rd Semaev polynomial and reduce it modulo given polynomial from Z[x1,x2]. Find x3,a,b so that remainder is zero """
    RX = PolynomialRing(field,['Y1','Y2','Y3','c','d'])
    Y1,Y2,Y3,c,d = RX.gens()
    relation_poly = relation_polynomial_edwards(RX)
    RXQ = RX.quotient(poly)
    f = RXQ(relation_poly)
    f_decomposition = {} # the dictionary is of the form (deg(X1),deg(X2)):list of [coefficient,deg(X3),deg(a),deg(b)]
    for monomial in f.monomials():
        monlift = monomial.lift()
        coef = f.lift().coefficient(monlift)
        key = (monlift.degrees()[0],monlift.degrees()[1])
        value = [coef,{"degY3":monlift.degrees()[2],"degc":monlift.degrees()[3],"degd":monlift.degrees()[4]}]
        if key in f_decomposition:
            f_decomposition[key].append(value)
        else:
            f_decomposition[key]=[value]
    EquationRing=PolynomialRing(field,['Y3','c','d'])
    Y3,c,d = EquationRing.gens()
    equations = set()
    for key,value in f_decomposition.items():
        g = 0
        for mon in value:
            coef = ZZ(mon[0].constant_coefficient())
            degrees = mon[1]
            g+=coef*Y3**degrees["degY3"]*c**degrees["degc"]*d**degrees["degd"]
        equations.add(EquationRing(g))
    equations = list(equations)
    I = EquationRing.ideal(equations)
    #if I.dimension()==-1:
    #    print("polynomial:",poly)
    #    print("dim:", I.dimension(),", ",I)
    #    print("NO SOLUTION")
    #    print('----')
    #    return
    if I.dimension()==0:
        if verbose:
            print("polynomial:", poly)
            print("dim:", I.dimension(),", ",I)
            print("SOLUTION",I.variety())
            print('----')
        return I.variety()
    if I.dimension()>0:
        if verbose:
            print("polynomial:",poly)
            print("dim:", I.dimension(),", ",I)
            print("MANY SOLUTIONS")
            print('----')
            return [None]
    return []

