inverted:add-2007-bl
zpa:{Y2*X1 + X2*X1 + X2*Y1}
rpa:{-Y2*X2*X1*Y1 + d, Y2*X2*X1*Y1 + d, X2*X1 - Y2*Y1, -X2*X1 + Y2*Y1, Y2*X1 + X2*Y1}
easy-nonrpa:{X1 + Y1, Y2 + X2}

inverted:madd-20080225-hwcd
zpa:{X2 + Y1, Y2 + Y1, Y2 + X1, X2 - X1}
rpa:{Y2*X2 + X1*Y1, -Y2*X2 + X1*Y1, X2*X1 + Y2*Y1, Y2*X1 - X2*Y1}
easy-nonrpa:set()

inverted:add-20080225-hwcd
zpa:{X2 + Y1, Y2 + Y1, Y2 + X1, X2 - X1}
rpa:{Y2*X2 + X1*Y1, -Y2*X2 + X1*Y1, X2*X1 + Y2*Y1, Y2*X1 - X2*Y1}
easy-nonrpa:set()

inverted:madd-2007-bl
zpa:{Y2*X1 + X2*X1 + X2*Y1}
rpa:{-Y2*X2*X1*Y1 + d, Y2*X2*X1*Y1 + d, X2*X1 - Y2*Y1, -X2*X1 + Y2*Y1, Y2*X1 + X2*Y1}
easy-nonrpa:{X1 + Y1, Y2 + X2}

inverted:mmadd-2007-bl
zpa:{X1*X2 + Y1*X2 + X1*Y2}
rpa:{Y1*X2 + X1*Y2, -X1*X2 + Y1*Y2, X1*Y1*X2*Y2 + d, -X1*Y1*X2*Y2 + d, X1*X2 - Y1*Y2}
easy-nonrpa:{X1 + Y1, X2 + Y2}

inverted:xmadd-2007-bl
zpa:set()
rpa:{Y2*X1 + Y1, Y2*Y1 - X1, Y2*X1*Y1 + d, -Y2*X1*Y1 + d}
easy-nonrpa:set()

