:ladd-1987-m-2
zpa:set()
rpa:{X2, X3*X2 - 1, X3 - X2, X1}
easy-nonrpa:{X3 - 1, X2 - 1, X3 + 1, X2^2 + X2*a + 1, X2 + 1}

:mladd-1987-m
zpa:set()
rpa:{X2, X3*X2 - 1, X3 - X2, X1}
easy-nonrpa:{X3 - 1, X2 - 1, X3 + 1, X2^2 + X2*a + 1, X2 + 1}

:ladd-1987-m-3
zpa:set()
rpa:{X2, X3*X2 - 1, X3 - X2, X1}
easy-nonrpa:{X3 - 1, X2 - 1, X3 + 1, X2^2 + X2*a + 1, X2 + 1}

:ladd-1987-m
zpa:set()
rpa:{X3 - X2, X3*X2 - 1, X2, X3, X1}
easy-nonrpa:{X2^2 + X2*a + 1, X2 + 1, X2 + a, X2 - 1}

