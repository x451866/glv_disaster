import json, re

def extract_variables(formula,filename):
    variable_names = set()
    for [left,right] in formula:
        for char in re.split(r'[a|b|c|d|\(|\*|\+|\-|\s|\/|\)]\s*',right.strip()):
            if char=='' or char.isdigit():
                continue
            variable_names.add(char)
        if "mont" in filename or "shortw" in filename:
            coefficient_names=["b","a"]
        if "twisted" in filename:
            coefficient_names=["d","a"]
        if "edwards" in filename:
            coefficient_names = ["d","c"]
    return list(variable_names), coefficient_names

def parse_to_json(filename):
    with open(filename) as f:
        txtlines = f.readlines()

    formulae = {}
    category = ""
    for line in txtlines:
        if line.strip()=="":
            continue
        if "######" in line:
            category = line.split("######")[1].strip()
            continue
        if "---" in line:
            formula_name = f'{category}:{line.split("---")[1].strip()}'
            formulae[formula_name]={"formula":[]}
            continue
        if " = " in line:
            left,right = line.split(" = ")
            formulae[formula_name]["formula"].append([left.strip(),right.strip()])
            continue
        print(f"Ignoring line: {line.strip()} in {filename}/{formula_name}")

    for formula_name in list(formulae.keys()):
        formula = formulae[formula_name]["formula"]
        if formula ==[]:
            del formulae[formula_name]
            continue
        formulae[formula_name]["variables"],formulae[formula_name]["coefficient_names"] =extract_variables(formula,filename)

    with open(f"{filename.split('.txt')[0]}.json","w") as f:
        json.dump(formulae,f)

filenames = ["unroll_add_edwards.txt","unroll_add_shortw.txt","unroll_add_twisted.txt","unroll_dadd_edwards.txt","unroll_dadd_montgom.txt","unroll_dadd_shortw.txt","unroll_ladd_edwards.txt","unroll_ladd_montgom.txt","unroll_ladd_shortw.txt"]
for filename in filenames:
    parse_to_json(filename)
