#!/usr/bin/env python
# coding: utf-8

# In[1]:


from formula import *


# In[2]:


add_edwards = load_formulae("unroll/unroll_add_edwards.json")
add_shortw = load_formulae("unroll/unroll_add_shortw.json")
add_twisted = load_formulae("unroll/unroll_add_twisted.json")
dadd_edwards = load_formulae("unroll/unroll_dadd_edwards.json")
dadd_montgom = load_formulae("unroll/unroll_dadd_montgom.json")
dadd_shortw = load_formulae("unroll/unroll_dadd_shortw.json")
ladd_edwards = load_formulae("unroll/unroll_ladd_edwards.json")
ladd_montgom = load_formulae("unroll/unroll_ladd_montgom.json")
ladd_shortw = load_formulae("unroll/unroll_ladd_shortw.json")


# In[33]:


"""
Edwards addition
"""
formulae = deepcopy(add_edwards)
for formula_name, formula in formulae.items():
    formula.to_affine()
    if formula.category()=="inverted":
        formula.invert_coordinates() 
    formula.remove_constants()
    formula.create_polynomial_set()
    formula.remove_rpa()
    formula.remove_single_variable()
    formula.remove_one_point_polynomials()
    #print(formula_name)
    #print(formula.polynomial_set)

#results_per_formula(formulae,"add_edwards")
polynomials = polynomial_set_all(formulae)
polynomials


# In[34]:


"""
Y1+Y2, Y2 + X1, Y1 + X2, X1 - X2, Y2+X2,X1+Y1 have been ruled out in
https://link.springer.com/content/pdf/10.1007/s00200-013-0211-2.pdf 
(they almost never appear only for special cases of points and scalars)
"""
X1,Y1,X2,Y2 = get_variables(polynomials,['X1','Y1','X2','Y2'])
to_remove = set([Y1+Y2, Y2 + X1, Y1 + X2, X1 - X2,Y2+X2,X1+Y1,X2-X1])
classify_formulae(formulae, to_remove)


# In[35]:


"""
Short Weierstrass addition
"""
formulae = deepcopy(add_shortw)
for formula_name, formula in formulae.items():
    formula.to_affine()
    formula.remove_constants()
    formula.create_polynomial_set()
    formula.remove_rpa()
    formula.remove_single_variable()
    formula.remove_one_point_polynomials()
    
results_per_formula(formulae,"add_weierstrass")
polynomials = polynomial_set_all(formulae)
polynomials


# In[36]:


"""
X1-X2 is trivial
"""
X1,Y1,X2,Y2 = get_variables(polynomials,['X1','Y1','X2','Y2'])
to_remove = set([X1-X2,X2-X1])
classify_formulae(formulae, to_remove)


# In[37]:


"""
Edwards twisted addition
"""
formulae = deepcopy(add_twisted)
for formula_name, formula in formulae.items():
    formula.to_affine()
    if formula.category()=="inverted":
        formula.invert_coordinates() 
    if formula.category() in ["extended","extended-1"]:
        formula.remove_extension()
    formula.remove_constants()
    formula.create_polynomial_set()
    formula.remove_rpa()
    formula.remove_single_variable()
    formula.remove_one_point_polynomials()
    #print(formula_name)
    #print(formula.polynomial_set)

results_per_formula(formulae,"add_twisted")
polynomials = polynomial_set_all(formulae)
polynomials


# In[38]:


"""
Y1+Y2, Y2 + X1, Y1 + X2, X1 - X2, Y2+X2,X1+Y1 have been ruled out in
https://link.springer.com/content/pdf/10.1007/s00200-013-0211-2.pdf 
(they almost never appear only for special cases of points and scalars)
"""
X1,Y1,X2,Y2 = get_variables(polynomials,['X1','Y1','X2','Y2'])
to_remove = set([Y1+Y2, Y2 + X1, Y1 + X2, X1 - X2,X2+Y2,X1+Y1,X2-X1,X1-Y1,Y2-X2])
classify_formulae(formulae, to_remove)


# In[39]:


"""
Edwards diff addition
"""
formulae = deepcopy(dadd_edwards)
for formula_name, formula in formulae.items():
    formula.to_affine()
    formula.remove_yz()
    formula.remove_constants()
    formula.create_polynomial_set()
    formula.remove_rpa()
    formula.remove_single_variable()
    formula.remove_one_point_polynomials()
    #print(formula_name)
    #print(formula.polynomial_set)

results_per_formula(formulae,"dadd_edwards")
polynomials = polynomial_set_all(formulae)
polynomials


# In[40]:


classify_formulae(formulae)


# In[41]:


"""
Short Weierstrass diff addition
"""
formulae = dadd_shortw
for formula_name, formula in formulae.items():
    formula.to_affine()
    formula.remove_constants()
    formula.create_polynomial_set()
    formula.remove_rpa()
    formula.remove_single_variable()
    formula.remove_one_point_polynomials()
    #print(formula_name)
    #print(formula.polynomial_set)

results_per_formula(formulae,"dadd_shortw")
polynomials = polynomial_set_all(formulae)
polynomials


# In[42]:


classify_formulae(formulae)


# In[43]:


"""
Montgomery diff addition
"""
formulae = deepcopy(dadd_montgom)
for formula_name, formula in formulae.items():
    formula.to_affine()
    formula.remove_constants()
    formula.create_polynomial_set()
    formula.remove_rpa()
    formula.remove_single_variable()
    formula.remove_one_point_polynomials()
    #print(formula_name)
    #print(formula.polynomial_set)

results_per_formula(formulae,"dadd_montgom")
polynomials = polynomial_set_all(formulae)
polynomials


# In[44]:


classify_formulae(formulae)


# In[45]:


"""
Short Weierstrass ladder
"""
formulae = deepcopy(ladd_shortw)
for formula_name, formula in formulae.items():
    formula.to_affine()
    formula.remove_constants()
    formula.create_polynomial_set()
    formula.remove_rpa()
    formula.remove_single_variable()
    formula.remove_one_point_polynomials()
    #print(formula_name)
    #print(formula.polynomial_set)

results_per_formula(formulae,"ladd_shortw")
polynomials = polynomial_set_all(formulae)
polynomials


# In[46]:


classify_formulae(formulae)


# In[47]:


"""
Montgomery ladder
"""
formulae = deepcopy(ladd_montgom)
for formula_name, formula in formulae.items():
    formula.to_affine()
    formula.remove_constants()
    formula.create_polynomial_set()
    formula.remove_rpa()
    formula.remove_single_variable()
    formula.remove_one_point_polynomials()
    #print(formula_name)
    #print(formula.polynomial_set)

results_per_formula(formulae,"ladd_montgom")
polynomial_set_all(formulae)


# In[48]:


classify_formulae(formulae)


# In[49]:


"""
Edwards ladder
"""
formulae = deepcopy(ladd_edwards)
for formula_name, formula in formulae.items():
    formula.to_affine()
    formula.remove_yz(squared=True)
    formula.remove_constants()
    formula.create_polynomial_set()
    formula.remove_rpa()
    formula.remove_single_variable()
    formula.remove_one_point_polynomials()
    #print(formula_name)
    #print(formula.polynomial_set)

results_per_formula(formulae,"ladd_edwards")
polynomial_set_all(formulae)


# In[50]:


classify_formulae(formulae)


# Categories with restrictions on curves

# In[31]:


"""
Short Weierstrass addition: w12-0
"""
formulae = deepcopy(add_shortw)
for formula_name, formula in list(formulae.items()):
    if formula.category()!="w12-0":
        formulae.pop(formula_name)
        continue
    formula.to_affine()
    formula.remove_constants()
    formula.create_polynomial_set()
    formula.remove_rpa()
    formula.remove_single_variable()
    formula.remove_one_point_polynomials()


polynomials = polynomial_set_all(formulae)
polynomials


# In[32]:


classify_formulae(formulae)


# In[29]:


"""
Edwards twisted addition: extended-1
"""
formulae = deepcopy(add_twisted)
for formula_name, formula in list(formulae.items()):
    if formula.category()!="extended-1":
        formulae.pop(formula_name)
        continue
    formula.remove_extension()
    formula.remove_constants()
    formula.create_polynomial_set()
    formula.remove_rpa()
    formula.remove_single_variable()
    formula.remove_one_point_polynomials()
    #print(formula_name)
    #print(formula.polynomial_set)

results_per_formula(formulae,"add_twisted")
polynomials = polynomial_set_all(formulae)
polynomials


# In[30]:


"""
Y1+Y2, Y2 + X1, Y1 + X2, X1 - X2, Y2+X2,X1+Y1 have been ruled out in
https://link.springer.com/content/pdf/10.1007/s00200-013-0211-2.pdf 
(they almost never appear only for special cases of points and scalars)
"""
X1,Y1,X2,Y2 = get_variables(polynomials,['X1','Y1','X2','Y2'])
to_remove = set([Y1+Y2, Y2 + X1, Y1 + X2, X1 - X2,X2+Y2,X1+Y1,X2-X1,X1-Y1,Y2-X2])
classify_formulae(formulae, to_remove)


# In[ ]:




