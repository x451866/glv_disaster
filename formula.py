import json
from sage.all import *
from copy import deepcopy

class Formula:
    """ 
    Class to represent an elliptic curve operation formula
    Takes a name of the formula and a dictionary on input. 
    The dictionary must have three keys:
        - 'variables': contains list of strings used as variables in the formula (not curve coefficients)
        - 'cofficient_names': contains list of strings representing curve coefficients (e.g., ['a','b'] for Weierstrass)
        - 'formula': contains a list of tuples (left, right) where left is any string and right is a string representing polynomial
            in variables from 'variables'. Each tuple represents expression left=right from the formula.
    
    Useful attribues:
        - name: name of the formula
        - variables: list of variables
        - coefficients: list of curve coefficients
        - ring: ZZ[variables+coefficients]
        - formula: list of tuples (left,right) where left is a string and right is an element from ring
        - polynomial_set: empty set if the method create_polynomial_set() is not called, see below.
        - single_variable: empty set if the method remove_single_variable() is not called, see below.
        - rpa: empty set if the method remove_rpa() is not called, see below.
        - one_point_polynomial: empty set if the method remove_one_point_polynomial() is not called, see below.
    
    """
    def __init__(self,name,dictionary):
        self.name = name
        self.variables = dictionary["variables"]
        self.coefficients = dictionary["coefficient_names"]
        self.ring = PolynomialRing(ZZ,self.variables+self.coefficients)
        self.formula = []
        for [left,right] in dictionary["formula"]:
            # get rid of denominators
            right_polynomial = self.ring(self.ring.fraction_field()(right).numerator())
            self.formula.append([left,right_polynomial])

        self.polynomial_set = set()
        self.single_variable = set()
        self.rpa = set()
        self.one_point_polynomial = set()
        
    def formula_dict(self):
        """ returns the formula as a dictionary """
        return dict(self.formula)
    
    def formula_print(self):
        """pretty print"""
        for [left,right] in self.formula:
            print(f"{left} = {right}")

    def category(self):
        """category of the formula (string before :)"""
        category = self.name.split(":")[0]
        return "n" if category=="" else category

    def result_polynomials(self):
        """sublist of formula attribute only with the tuples corresponding to the final result"""
        result = []
        for var_prefix in ['X','Y','Z']:
            try:
                var = sorted(filter(lambda x: var_prefix==x[0] and x[1:].isdigit(),self.formula_dict().keys()))[-1]
                result.append([var,self.formula_dict()[var]])
            except IndexError:
                continue
        return result
        
    def to_affine(self):
        """ transforms the formula to an affine form"""
        new_formula = []
        for [left,right] in self.formula:
            new_formula.append([left,right(*[1 if 'Z' in str(g) else g for g in self.ring.gens()])])
        self.formula = new_formula

    def invert_coordinates(self):
        """ invert coordinates (used for inverted-coordinates formulae)"""
        inverted_formula = []
        for [left,right] in self.formula:
            right_inverted = right(*[1/var for var in self.ring.gens()])
            inverted_formula.append([left,self.ring(right_inverted.numerator())])
        self.formula = inverted_formula

    def remove_extension(self):
        """ get rid of extended coordinates"""
        for g in ['T1','T2']: self.variables.remove(g)
        X1,X2,Y1,Y2,T1,T2 = (self.ring.gens_dict()[g] for g in ['X1','X2','Y1','Y2','T1','T2']) 
        self.ring = PolynomialRing(ZZ,self.variables+self.coefficients)
        formula  = []
        for [left,right] in self.formula:
            # get rid of T1,T2
            right_polynomial = self.ring(right.substitute({T1:X1*Y1,T2:X2*Y2}) )
            formula.append([left,right_polynomial])
        self.formula = formula

    def remove_yz(self,squared=False):
        """ get rid of yz coordinates """
        self.coefficients, self.variables = ['r'], ['Y1','Y2','Y3']
        Y1,Y2,Y3,c,d,r = (self.ring.gens_dict()[g] for g in ['Y1','Y2','Y3','d','c','r'])
        self.ring = PolynomialRing(ZZ,self.variables+self.coefficients)
        formula  = []
        for [left,right] in self.formula:
            right_polynomial = self.ring(right.substitute({Y1:r*Y1**(squared+1),Y2:r*Y2**(squared+1),Y3:r*Y3**(squared+1),c:1,d:r**2}))
            formula.append([left,right_polynomial])
        self.formula = formula 



    def remove_constants(self):
        """ removes constant multiples for each line in the formula except for polynomials in coefficients"""
        new_formula = []
        for left, right in self.formula:
            if right.is_constant():
                continue
            if set(str(v) for v in right.variables()).issubset(set(self.coefficients)):
                continue
            new_formula.append([left,right/gcd(right.coefficients())])
        self.formula = new_formula
    
        
    def create_polynomial_set(self):
        """
        takes all right-hand side polynomials and returns a set of all of the factors
        remove also all factors that are polynomials in coefficients (constants)
        saves the result into polynomial_set attribute
        """
        for [left,right] in self.formula:
            for f,ex in right.factor():
                # check whether the polynomial is not constant
                if set(str(v) for v in f.variables()).issubset(set(self.coefficients)):
                    continue
                self.polynomial_set.add(f*(-1)**(f.coefficient(f.variables()[0])<0))
        return self.polynomial_set

                
    def remove_single_variable(self):
        """
        removes all polynomials from the polynomial set that are in one variable
        """
        new_formula_set = set()
        for f in list(self.polynomial_set):
            variables_without_coefficients = set(f.variables())-set(self.ring.gens_dict()[c] for c in self.coefficients)
            if len(variables_without_coefficients)>1:
                new_formula_set.add(f)
            else:
                self.single_variable.add(f)
        self.polynomial_set = new_formula_set
        return self.polynomial_set
    
    def remove_one_point_polynomials(self):
        """
        removes all polynomials that depend on only one point
        """
        new_formula_set = set()
        for f in list(self.polynomial_set):
            if len(set([str(v)[1:] for v in f.variables() if str(v) not in self.coefficients]))>1:
                new_formula_set.add(f)
            else:
                self.one_point_polynomial.add(f)
        self.polynomial_set = new_formula_set
        return self.polynomial_set
            
    def remove_rpa(self):
        """
        removes all polynomials from the polynomial set that lead to an rpa attack (they divide the resulting polynomials)
        and polynomials equal to the input variables (corresponding to input points with zero variable)
        """
        for result_right in [r[1] for r in self.result_polynomials()]:
            for f in list(self.polynomial_set):
                if f.divides(result_right) or f in self.ring.gens():
                    self.polynomial_set.discard(f)
                    self.rpa.add(f)
        return self.polynomial_set

    
    def filter_polynomials(self,filter_map):
        """
        removes polynomials for which the given filter_map outputs True
        """
        for f in list(self.polynomial_set):
            if filter_map(f):
                self.polynomial_set.discard(f)
        return self.polynomial_set
    
    
def load_formulae(path):
    """
    Loads all the formulas from prepared json files. See 'parse_to_json.py'
    """
    with open(path) as f:
        formulae_strings = json.load(f)
    
    formulae = {}
    for formula_name, formula in formulae_strings.items():
        R = PolynomialRing(ZZ,formula['variables']+formula["coefficient_names"])
        formulae[formula_name] = Formula(formula_name,deepcopy(formula))
    
    return formulae

def polynomial_set_all(formulae,string=False):
    """
    Create a set of polynomials from all formulae
    """
    all_polynomials = set()
    variables = set()
    for formula in formulae.values():
        variables.update(formula.variables+formula.coefficients)
    all_ring = PolynomialRing(ZZ,list(variables))
    for formula in formulae.values():
        for f in formula.polynomial_set:
            g = all_ring(f)
            all_polynomials.add(g*(-1)**(g.coefficient(g.variables()[0])<0))
    if string:
        return str(all_polynomials).replace("^","**")
    return all_polynomials

def results_per_formula(formulae,form):
    """Prints results of analysis to text files"""
    categories = set()
    for formula_name, formula in formulae.items():
        categories.add(formula.category())
    for category in list(categories):
        result = ""
        for formula_name, formula in formulae.items():
            if formula.category()==category:
                result+=f"{formula_name}\nzpa:{formula.polynomial_set}\nrpa:{formula.rpa}\neasy-nonrpa:{formula.single_variable.union(formula.one_point_polynomial)}\n\n"
        with open(f"results/{form}_{category}.txt","w") as f:
            f.write(result)


def remove_polynomials(polynomials,to_remove):
    """Special function to subtract sets of polynomials 
    (the polynomials might have rings in inclusion so plain '-' does not work)"""
    if to_remove==set():
        return polynomials
    ring = list(to_remove)[0].parent()
    polynomials = set([ring(f) for f in list(polynomials)])
    return polynomials-to_remove


def classify_formulae(formulae, to_remove=[]):

    # we need to convert all polynomials into the same ring
    variables = set()
    for formula in formulae.values():
        variables.update(formula.variables+formula.coefficients)
    all_ring = PolynomialRing(ZZ,list(variables))
    to_remove = set(all_ring(f) for f in to_remove)

    easyzpa, hardzpa, nozpa = [],[],[]
    for formula_name, formula in formulae.items():
        polynomial_set = set(all_ring(f) for f in formula.polynomial_set if all_ring(f) not in to_remove)
        easyzpaset = set(all_ring(f) for f in formula.single_variable.union(formula.one_point_polynomial) \
            if all_ring(f) not in to_remove)
        if easyzpaset!=set():
            easyzpa.append((formula_name,easyzpaset,polynomial_set))
            continue
        if polynomial_set==set():
            nozpa.append(formula_name)
            continue
        hardzpa.append((formula_name,polynomial_set))
    print("**No ZPA:**")
    for formula_name in nozpa: print(formula_name,end=", ")
    print("\n\n**Hard ZPA:**")
    all_poly_list = []
    for _, poly_set in hardzpa:
        if poly_set not in all_poly_list:
            all_poly_list.append(poly_set)
    for poly_set in all_poly_list:
        for formula_name, poly_set_i in hardzpa:
            if poly_set_i==poly_set:
                print(formula_name,end=", ")
        print(poly_set,"\n")
    print("**Easy ZPA:**")
    for formula_name, poly_set, hard in easyzpa: print(f"{formula_name}: {poly_set}, hard:{hard}\n")
        

def get_variables(polynomials,names):
    return [list(polynomials)[0].parent().gens_dict()[i] for i in names]


def remove_y(poly,a,b,Y1,Y2,X1,X2):
    """not using"""
    if poly.degree(Y1)==poly.degree(Y2)==0:
        return poly
    f0 = poly.constant_coefficient()
    f12 = poly.coefficient({Y1:1,Y2:1})
    f1 = poly.coefficient({Y1:1,Y2:0})
    f2 = poly.coefficient({Y2:1,Y1:0})
    curve1, curve2 = X1**3+a*X1+b,X2**3+a*X2+b
    return curve2*(curve1*2*f1*f12-2*f0*f2)**2-(f0**2+f2**2*curve2-curve1*(f1**2+f12**2*curve2))**2

def test_remove_y():
    f = 1+2*Y1+3*Y2+4*Y1*Y2
    a,b = 0,1
    expected_fprime = -256*X1**6*X2**6 - 384*X1**6*X2**3 - 224*X1**3*X2**6 - 144*X1**6 - 280*X1**3*X2**3 - 49*X2**6 - 80*X1**3 - 40*X2**3
    assert expected_fprime==remove_y(f,a,b,Y1,Y2,X1,X2)
    assert X1 == remove_y(X1,a,b,Y1,Y2,X1,X2)
