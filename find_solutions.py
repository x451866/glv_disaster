#!/usr/bin/env python
# coding: utf-8
# ---
# jupyter:
#   jupytext:
#     cell_metadata_filter: -all
#     custom_cell_magics: kql
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.11.2
#   kernelspec:
#     display_name: Python 3.8.10 64-bit
#     language: python
#     name: python3
# ---

# %%


import curve_forms as cf
from sage.all import *
import importlib
importlib.reload(cf)


# %%


def brute_force_solution(E,f,easy_case = None):
    for P in E:
        for Q in E:
            if f(X1=P.x,Y1=P.y,X2=Q.x,Y2=Q.y)==0:
                try:
                    Pw = P.to_weierstrass().sage_point()
                    Qw = Q.to_weierstrass().sage_point()
                    R = P-Q
                    S = P+Q
                    Rw = Pw-Qw
                    Sw = Pw+Qw
                    if easy_case is not None and easy_case(Pw,Qw,P,Q,R,S):
                        continue
                except ZeroDivisionError:
                    #print("exceptional points")
                    continue
                print(f"P=({P.x},{P.y}),Q=({Q.x},{Q.y}), P-Q=({R.x},{R.y}), P+Q=({S.x},{S.y}), orders:{Pw.order()},{Qw.order()},{Rw.order()},{Sw.order()}",end=", DLP:")
                try:
                    print(f"{Pw.discrete_log(Qw)}")
                except ValueError:
                    print("none")
    print("END")


# %%


"""
Edwards addition (inverted and projective)
"""

F = GF(37)
E = cf.Edwards(F(5),F(22))#cf.find_random_edwards(F)
print(F(1+2/E.d).is_square(),(F(1+1/E.d)).is_square())
print(E, E.to_weierstrass().sage_curve.order())
X1,Y1,X2,Y2,c,d = PolynomialRing(F,['X1','Y1','X2','Y2','c','d']).gens()
edwards_formulae = [d*X2*X1*Y2*Y1 + 2, 2*d*X2*X1*Y2*Y1 + 1]#[Y2 + Y1, X1 + Y2, X2 + Y1, X2 - X1, X1*Y2 + X2*Y1 + Y2*Y1, \
                    #d*X2*X1*Y2*Y1 + 2, 2*d*X2*X1*Y2*Y1 + 1]


# Based on https://link.springer.com/content/pdf/10.1007/s00200-013-0211-2.pdf
def easy_case(Pw,Qw,P,Q,R,S):
    # Theorems 3, 4 and 5
    if Pw.order()<=8 or Qw.order()<=8 or Pw.curve()(0) in [Qw,Qw+Pw,2*Qw,2*Qw+2*Pw,4*Qw,4*Qw+4*Pw,8*Qw]:
        return True
    return 0 in set([P.x,P.y,Q.x,Q.y,R.x,R.y,S.x,S.y])

        
    
for f in edwards_formulae:
    print(f)
    brute_force_solution(E,f(c=E.c,d=E.d))#,easy_case)


# %%


"""
Short Weierstrass addition: all
"""

F = GF(103)
E = cf.find_random_weierstrass(F) 
print(E)
X1,Y1,X2,Y2,a,b = PolynomialRing(F,['X1','Y1','X2','Y2','a','b']).gens()
weierstrass_formulae = {X1**2 + X1*X2 + X2**2 + a, X1*Y2 + X2*Y1, Y2**2 + 2*Y2*Y1 + Y1**2 + 2*X1 + 2*X2, X1**3 - 3*X1**2*X2 + 3*X1*X2**2 - X2**3 + Y2**2 - 2*Y2*Y1 + Y1**2, 3*X1*X2 + a, X1 - X2 - 1, X1**2 + X1*X2 + X2**2 - 1, X1**2 + X1*X2 + X2**2, X1 - X2, -2*X1**3 + 3*X1**2*X2 - X2**3 + Y2**2 - 2*Y2*Y1 + Y1**2, X1*X2 - 1, 2*X1**4 + 4*X1**3*X2 + 6*X1**2*X2**2 + 4*X1*X2**3 + 2*X2**4 + 4*X1**2*a + 4*X1*a*X2 + 4*a*X2**2 - 3*X1*Y2**2 - 3*X2*Y2**2 - 6*X1*Y2*Y1 - 6*X2*Y2*Y1 - 3*X1*Y1**2 - 3*X2*Y1**2 + 2*a**2, -X1*X2 + X1*b + X2*b - 3, -2*X1**4 - 4*X1**3*X2 - 6*X1**2*X2**2 - 4*X1*X2**3 - 2*X2**4 + 3*X1*Y2**2 + 3*X2*Y2**2 + 6*X1*Y2*Y1 + 6*X2*Y2*Y1 + 3*X1*Y1**2 + 3*X2*Y1**2 + 4*X1**2 + 4*X1*X2 + 4*X2**2 - 2, -X1**3 + 2*X1**2*X2 - X1*X2**2 + Y2**2 - 2*Y2*Y1 + Y1**2, -X1*X2 + a, Y2 + Y1, X1 - X2 - Y2 + Y1, -X1*a*X2 + a**2 - 3*X1*b - 3*X2*b, 2*X1 - 2*X2 - Y2 + Y1, X1 + X2 - 1, X1 + X2, X1 + X2 + 1, X1 - X2 - 2, Y2**2 + 2*Y2*Y1 + Y1**2 + X1 + X2, Y2 - Y1}
def easy_case(Pw,Qw,P,Q,R,S):
    return Pw[0]==Qw[0] or 0 in set(Pw[:3]+Qw[:3]+(Pw+Qw)[:3]+(Pw-Qw)[:3])

        
for f in list(weierstrass_formulae):
    print(f)
    brute_force_solution(E,f(a=E.a,b=E.b),easy_case)


# %%


"""
Short Weierstrass addition: jacobian-3
"""

F = GF(103)
E = cf.find_random_weierstrass(F,a=F(-3)) 
print(E)
X1,Y1,X2,Y2,a,b = PolynomialRing(F,['X1','Y1','X2','Y2','a','b']).gens()
weierstrass_formulae = {X1 - X2 - 2, X1 + X2, Y2 - Y1, X1**3 - 3*X1**2*X2 + 3*X1*X2**2 - X2**3 + Y2**2 - 2*Y2*Y1 + Y1**2, -X1**3 + 2*X1**2*X2 - X1*X2**2 + Y2**2 - 2*Y2*Y1 + Y1**2, X1 - X2 - 1, -2*X1**3 + 3*X1**2*X2 - X2**3 + Y2**2 - 2*Y2*Y1 + Y1**2}

def easy_case(Pw,Qw,P,Q,R,S):
    return Pw[0]==Qw[0] or 0 in set(Pw[:3]+Qw[:3]+(Pw+Qw)[:3]+(Pw-Qw)[:3])

        
for f in list(weierstrass_formulae):
    print(f)
    brute_force_solution(E,f(a=E.a,b=E.b),easy_case)


# %%


"""
Short Weierstrass addition: jacobian-0
"""

F = GF(103)
E = cf.find_random_weierstrass(F,a=F(0))
print(E)
X1,Y1,X2,Y2,a,b = PolynomialRing(F,['X1','Y1','X2','Y2','a','b']).gens()
weierstrass_formulae = {X1 - X2 - 2, X1 + X2, Y2 - Y1, X1**3 - 3*X1**2*X2 + 3*X1*X2**2 - X2**3 + Y2**2 - 2*Y2*Y1 + Y1**2, -X1**3 + 2*X1**2*X2 - X1*X2**2 + Y2**2 - 2*Y2*Y1 + Y1**2, X1 - X2 - 1, -2*X1**3 + 3*X1**2*X2 - X2**3 + Y2**2 - 2*Y2*Y1 + Y1**2}

def easy_case(Pw,Qw,P,Q,R,S):
    return Pw[0]==Qw[0] or 0 in set(Pw[:3]+Qw[:3]+(Pw+Qw)[:3]+(Pw-Qw)[:3])

        
for f in list(weierstrass_formulae):
    print(f)
    brute_force_solution(E,f(a=E.a,b=E.b),easy_case)


# %%


"""
Short Weierstrass addition: w12-0
"""

F = GF(53)
E = cf.find_random_weierstrass(F,b=F(0),cofactor=None)
print(E)
X1,Y1,X2,Y2,a,b = PolynomialRing(F,['X1','Y1','X2','Y2','a','b']).gens()
weierstrass_formulae = {X1 - X2 - Y2 + Y1, 2*X1 - 2*X2 - Y2 + Y1, X1 + X2, Y2 - Y1, -2*X1**3 + 3*X1**2*X2 - X2**3 + Y2**2 - 2*Y2*Y1 + Y1**2}

def easy_case(Pw,Qw,P,Q,R,S):
    return Pw[0]==Qw[0] or 0 in set(Pw[:3]+Qw[:3]+(Pw+Qw)[:3]+(Pw-Qw)[:3])

        
for f in list(weierstrass_formulae):
    print(f)
    brute_force_solution(E,f(a=E.a,b=E.b),easy_case)


# %%


"""
Short Weierstrass addition: xyzz-3
"""

F = GF(103)
E = cf.find_random_weierstrass(F,a = F(-3))
print(E)
X1,Y1,X2,Y2,a,b = PolynomialRing(F,['X1','Y1','X2','Y2','a','b']).gens()
weierstrass_formulae = {-2*X1**3 + 3*X1**2*X2 - X2**3 + Y2**2 - 2*Y2*Y1 + Y1**2, X1 - X2, X1**3 - 3*X1**2*X2 + 3*X1*X2**2 - X2**3 + Y2**2 - 2*Y2*Y1 + Y1**2, Y2 - Y1}

def easy_case(Pw,Qw,P,Q,R,S):
    return Pw[0]==Qw[0] or 0 in set(Pw[:3]+Qw[:3]+(Pw+Qw)[:3]+(Pw-Qw)[:3])

        
for f in list(weierstrass_formulae):
    print(f)
    brute_force_solution(E,f(a=E.a,b=E.b),easy_case)


# %%


"""
Short Weierstrass addition: projective-3
"""

F = GF(103)
E = cf.find_random_weierstrass(F,a = F(-3))
print(E)
X1,Y1,X2,Y2,a,b = PolynomialRing(F,['X1','Y1','X2','Y2','a','b']).gens()
weierstrass_formulae = {Y2 + Y1, X1**2 + X1*X2 + X2**2 + a, -X1*X2 + X1*b + X2*b - 3, -2*X1**3 + 3*X1**2*X2 - X2**3 + Y2**2 - 2*Y2*Y1 + Y1**2, X1**2 + X1*X2 + X2**2, X1*Y2 + X2*Y1, X1 + X2, Y2**2 + 2*Y2*Y1 + Y1**2 + 2*X1 + 2*X2, Y2 - Y1, X1**3 - 3*X1**2*X2 + 3*X1*X2**2 - X2**3 + Y2**2 - 2*Y2*Y1 + Y1**2, Y2**2 + 2*Y2*Y1 + Y1**2 + X1 + X2, X1*X2 - 1, 2*X1**4 + 4*X1**3*X2 + 6*X1**2*X2**2 + 4*X1*X2**3 + 2*X2**4 + 4*X1**2*a + 4*X1*a*X2 + 4*a*X2**2 - 3*X1*Y2**2 - 3*X2*Y2**2 - 6*X1*Y2*Y1 - 6*X2*Y2*Y1 - 3*X1*Y1**2 - 3*X2*Y1**2 + 2*a**2}

def easy_case(Pw,Qw,P,Q,R,S):
    return Pw[0]==Qw[0] or 0 in set(Pw[:3]+Qw[:3]+(Pw+Qw)[:3]+(Pw-Qw)[:3])

        
for f in list(weierstrass_formulae):
    print(f)
    brute_force_solution(E,f(a=E.a,b=E.b),easy_case)


# %%


"""
Short Weierstrass addition: projective-1
"""

F = GF(103)
E = cf.find_random_weierstrass(F,a = F(-1))
print(E)
X1,Y1,X2,Y2,a,b = PolynomialRing(F,['X1','Y1','X2','Y2','a','b']).gens()
weierstrass_formulae = {X1**2 + X1*X2 + X2**2 + a, -2*X1**4 - 4*X1**3*X2 - 6*X1**2*X2**2 - 4*X1*X2**3 - 2*X2**4 + 3*X1*Y2**2 + 3*X2*Y2**2 + 6*X1*Y2*Y1 + 6*X2*Y2*Y1 + 3*X1*Y1**2 + 3*X2*Y1**2 + 4*X1**2 + 4*X1*X2 + 4*X2**2 - 2, X1**2 + X1*X2 + X2**2 - 1, -2*X1**3 + 3*X1**2*X2 - X2**3 + Y2**2 - 2*Y2*Y1 + Y1**2, X1**2 + X1*X2 + X2**2, X1 + X2 - 1, X1 + X2, Y2**2 + 2*Y2*Y1 + Y1**2 + 2*X1 + 2*X2, X1**3 - 3*X1**2*X2 + 3*X1*X2**2 - X2**3 + Y2**2 - 2*Y2*Y1 + Y1**2, X1 + X2 + 1, Y2**2 + 2*Y2*Y1 + Y1**2 + X1 + X2, Y2 - Y1, 2*X1**4 + 4*X1**3*X2 + 6*X1**2*X2**2 + 4*X1*X2**3 + 2*X2**4 + 4*X1**2*a + 4*X1*a*X2 + 4*a*X2**2 - 3*X1*Y2**2 - 3*X2*Y2**2 - 6*X1*Y2*Y1 - 6*X2*Y2*Y1 - 3*X1*Y1**2 - 3*X2*Y1**2 + 2*a**2}

def easy_case(Pw,Qw,P,Q,R,S):
    return Pw[0]==Qw[0] or 0 in set(Pw[:3]+Qw[:3]+(Pw+Qw)[:3]+(Pw-Qw)[:3])

        
for f in list(weierstrass_formulae):
    print(f)
    brute_force_solution(E,f(a=E.a,b=E.b),easy_case)


# %%


F = GF(103)
E = cf.find_random_twistededwards(F)
print(E)
X1,Y1,X2,Y2,a,d = PolynomialRing(F,['X1','Y1','X2','Y2','a','d']).gens()

twistededwards_formulae = {X1*Y2 + X2*Y1 + Y2*Y1, X1*X2 + X1*Y2 - X2*Y1}


def easy_case(Pw,Qw,P,Q,R,S):
    return 0 in set(Pw[:3]+Qw[:3]+(Pw+Qw)[:3]+(Pw-Qw)[:3])
    return Pw==Qw or Pw==-Qw or 0 in set(Pw[:3]+Qw[:3]+(Pw+Qw)[:3]+(Pw-Qw)[:3])

        
for f in list(twistededwards_formulae):
    print(f)
    brute_force_solution(E,f(a=E.a,d=E.d),easy_case)


# %%


"""
Short Weierstrass diff. addition
"""

F = GF(103)
E = cf.find_random_weierstrass(F) 
print(E)
X1,Y1,X2,Y2,a,b = PolynomialRing(F,['X1','Y1','X2','Y2','a','b']).gens()
weierstrass_formulae = {X2 + X1, -X2*X1 + a, X2*X1 + a, X2**2*X1 + X2*X1**2 + X2*a + X1*a + 2*b}

def easy_case(Pw,Qw,P,Q,R,S):
    return Pw[0]==Qw[0] or 0 in set(Pw[:3]+Qw[:3]+(Pw+Qw)[:3]+(Pw-Qw)[:3])

        
for f in list(weierstrass_formulae):
    print(f)
    brute_force_solution(E,f(a=E.a,b=E.b),easy_case)


# %%


"""
Short Weierstrass ladder
"""

F = GF(103)
E = cf.find_random_weierstrass(F) 
print(E)
X1,Y1,X2,Y2,a,b = PolynomialRing(F,['X1','Y1','X2','Y2','a','b']).gens()
weierstrass_formulae = {X2 + X1, -X2*X1 + a, X2*X1 + a, X2**2*X1 + X2*X1**2 + X2*a + X1*a + 2*b}

def easy_case(Pw,Qw,P,Q,R,S):
    return Pw[0]==Qw[0] or 0 in set(Pw[:3]+Qw[:3]+(Pw+Qw)[:3]+(Pw-Qw)[:3])

        
for f in list(weierstrass_formulae):
    print(f)
    brute_force_solution(E,f(a=E.a,b=E.b),easy_case)


# %%




