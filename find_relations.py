#!/usr/bin/env python
# coding: utf-8

# In[1]:


import relations
import importlib
from formula import *
importlib.reload(relations)
from sage.all import *

add_shortw = load_formulae("unroll/unroll_add_shortw.json")
formulae = deepcopy(add_shortw)
for formula_name, formula in formulae.items():
    formula.to_affine()
    formula.remove_constants()
    formula.create_polynomial_set()
    formula.remove_rpa()
    formula.remove_single_variable()
    formula.remove_one_point_polynomials()
    
results_per_formula(formulae,"add_weierstrass")
hard_polynomials = polynomial_set_all(formulae)

p = 1031
# Apply the strategy from above
for f in hard_polynomials: 
    if f.degree(Y1)==f.degree(Y2)==0: # ignoring polynomials containing y1,y2 for now
        relations.relation_ideal(f,l=1,field=GF(p.next_prime()))


# In[4]:


for ex,polies in database.items():
    for poly in polies:
        if poly==-X1*X2+a:
            print(ex)
            break


# In[1]:


import relations
import importlib
importlib.reload(relations)
# Load formulas from https://github.com/crocs-muni/formula-for-disaster/tree/main/unrolling/unroll
expressions = relations.load_expressions("unroll/unroll_dadd_shortw.txt")
affine_expressions, database = relations.simplify_formulae(expressions, get_database=True)
reduced = copy(affine_expressions)
X1,X2,Z1,Z2,Y1,Y2,a,b = list(reduced)[0].parent().gens()
reduced = reduced-set([a,b,X1,X2,Y1,Y2,Y1+Y2,X1-X2]) #remove trivial cases

# Apply the strategy from above
for f in reduced: 
    if f.degree(Y1)==f.degree(Y2)==0: # ignoring polynomials containing y1,y2 for now
        relations.relation_ideal(f,l=1,field=GF((2**20).next_prime()))


# In[2]:


for ex,polies in database.items():
    for poly in polies:
        if poly==-X1^2*X2^2 + 2*X1*X2*a - a^2 + 4*X1*b + 4*X2*b:
            print(ex)
            break


# In[49]:


import relations
import importlib
importlib.reload(relations)
# Load formulas from https://github.com/crocs-muni/formula-for-disaster/tree/main/unrolling/unroll
expressions = relations.load_expressions("unroll/unroll_ladd_shortw.txt")
affine_expressions, database = relations.simplify_formulae(expressions, get_database=True)
reduced = copy(affine_expressions)
X1,X2,Z1,Z2,Y1,Y2,a,b = list(reduced)[0].parent().gens()
reduced = reduced-set([a,b,X1,X2,Y1,Y2,Y1+Y2,X1-X2]) #remove trivial cases

# Apply the strategy from above
for f in reduced: 
    if f.degree(Y1)==f.degree(Y2)==0: # ignoring polynomials containing y1,y2 for now
        relations.relation_ideal(f,l=1,field=GF((2**20).next_prime()))


# In[34]:


for ex,polies in database.items():
    for poly in polies:
        if poly==-X1*X2+a:
            print(ex)
            break


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# f(P,Q) <-> P+Q+R = 0
# Q = kP
# 
# 
# bit==1:
#     (0,neco) = Q+P
# 
# f(x1,x2,x3)

# f(x1,x2)=x1+x2 <-> P+Q+(0,neco)=0 nebo -P+Q+(0,neco)=0 nebo P-Q+(0,neco)=0

# In[5]:


f(x1,x2) = (x1-p)^2-ns*(x2-q)^2
D = (-ns*2*q)^2+4*((ns)*(x1-p)^2-(ns*q)^2)
D/4 = ns*(x1-p)^2


# In[ ]:


#g(x1)=nectverec skoro vzdy
ns*(x1-p)^2*(x1-c)^2*(x1-d)^2

