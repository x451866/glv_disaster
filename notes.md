```python
from sage.all import *
from glv import glv_decompose_simple,\
glv_decompose,glv_decompose_two,find_lambda,\
find_sigma, find_curve, dcp_instance, semaev
```

**Bitcoin curve - basic properties**

$p = 2^{256}-2^{32}-2^9-2^8-2^7-2^6-2^4-1$

$E: y^2=x^3+7$

CM - discriminant = $-3$

Frobenius discriminant = $-1 \cdot 3^3 \cdot 79^2 \cdot 349^2 \cdot 2698097^2 \cdot 1359580455984873519493666411^2$

Endomorphism algebra: 
- $K = \mathbb{Q}(\sqrt{-3})$, $O_K=\mathbb{Z}[\omega]$, $\omega = \frac{1+\sqrt{-3}}{2}$ 
- $O_K^{\times} = \{\pm 1, \pm \omega,\pm \omega^2\}$
- $(\beta x,y)$ as an endomorphism has order 3 for $\beta=\sqrt[3]{1}$. We can identify $(\beta x,y)=\omega$ and get $End(E)=O_K$
- $\omega$ acts as a scalar $\lambda$ on $E(\mathbb{F}_p)$ which satisfies $\lambda^2+\lambda+1=0 \pmod n$




```python
p = 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFC2F
a = 0
b = 7
F = GF(p)
E = EllipticCurve(F,[a,b])
n = E.order()
print(E)
lamb,beta = find_lambda(E)
print(f"beta: {beta}")
print(f"lambda: {lamb}")
```

    Elliptic Curve defined by y^2 = x^3 + 7 over Finite Field of size 115792089237316195423570985008687907853269984665640564039457584007908834671663
    beta: 60197513588986302554485582024885075108884032450952339817679072026166228089408
    lambda: 78074008874160198520644763525212887401909906723592317393988542598630163514318


    Elliptic Curve defined by y^2 = x^3 + 7 over Finite Field of size 115792089237316195423570985008687907853269984665640564039457584007908834671663
    beta: 60197513588986302554485582024885075108884032450952339817679072026166228089408
    lambda: 78074008874160198520644763525212887401909906723592317393988542598630163514318

**Bitcoin curve - Twists**

Twists of $E$ are of the form $E_{\mu}:y^2=x^3+\mu$ where $\mu \in \mathbb{F}_p\setminus(\mathbb{F}_p^{\times})^6$. If we identify curves isomorphic over $\mathbb{F}_p$ we get six isomorphic classes $\mathbb{F}_p/(\mathbb{F}_p^{\times})^6$ that can be represented, for example, by $\{\pm 1, \pm 7, \pm 49\}$. Overall, we have:

| $E_1$  | $E_7$  | $E_{49}$  |
|---|---|---|
| $E_{-1}$  |$E_{-7}$   |$E_{-49}$   |

 - horizontal isomorphisms are $(\sqrt[3]{7}x,\sqrt{7}y)$ and vertical are  $(\gamma x,iy)$  where $\gamma^2=\beta$
 - isomorphisms are defined over the second extension (see the maps)
 - Frobenius traces of $E_j$ and $E_{-j}$ are opposite
 - The group $E(\mathbb{F}_{p^2})$ is a (group) product of $E_j(\mathbb{F}_p)$ and $E_{-j}(\mathbb{F}_p)$ 



```python
E_1 = EllipticCurve(F,[0,1])
E_49 = EllipticCurve(F,[0,49])
E_m1 = EllipticCurve(F,[0,-1])
E_m7 = EllipticCurve(F,[0,-7])
E_m49 = EllipticCurve(F,[0,-49])

print(f"E_1: {E_1}")
print(f"E_7: {n}")
print(f"E_49: {E_49}")
print(f"E_m1: {E_m1}")
print(f"E_m7: {E_m7}")
print(f"E_m49: {E_m49}")

```

    E_1: Elliptic Curve defined by y^2 = x^3 + 1 over Finite Field of size 115792089237316195423570985008687907853269984665640564039457584007908834671663
    E_7: 115792089237316195423570985008687907852837564279074904382605163141518161494337
    E_49: Elliptic Curve defined by y^2 = x^3 + 49 over Finite Field of size 115792089237316195423570985008687907853269984665640564039457584007908834671663
    E_m1: Elliptic Curve defined by y^2 = x^3 + 115792089237316195423570985008687907853269984665640564039457584007908834671662 over Finite Field of size 115792089237316195423570985008687907853269984665640564039457584007908834671663
    E_m7: Elliptic Curve defined by y^2 = x^3 + 115792089237316195423570985008687907853269984665640564039457584007908834671656 over Finite Field of size 115792089237316195423570985008687907853269984665640564039457584007908834671663
    E_m49: Elliptic Curve defined by y^2 = x^3 + 115792089237316195423570985008687907853269984665640564039457584007908834671614 over Finite Field of size 115792089237316195423570985008687907853269984665640564039457584007908834671663


    E_1: Elliptic Curve defined by y^2 = x^3 + 1 over Finite Field of size 115792089237316195423570985008687907853269984665640564039457584007908834671663
    E_7: 115792089237316195423570985008687907852837564279074904382605163141518161494337
    E_49: Elliptic Curve defined by y^2 = x^3 + 49 over Finite Field of size 115792089237316195423570985008687907853269984665640564039457584007908834671663
    E_m1: Elliptic Curve defined by y^2 = x^3 + 115792089237316195423570985008687907853269984665640564039457584007908834671662 over Finite Field of size 115792089237316195423570985008687907853269984665640564039457584007908834671663
    E_m7: Elliptic Curve defined by y^2 = x^3 + 115792089237316195423570985008687907853269984665640564039457584007908834671656 over Finite Field of size 115792089237316195423570985008687907853269984665640564039457584007908834671663
    E_m49: Elliptic Curve defined by y^2 = x^3 + 115792089237316195423570985008687907853269984665640564039457584007908834671614 over Finite Field of size 115792089237316195423570985008687907853269984665640564039457584007908834671663

**Bitcoin curve - GLV** 

Any scalar $k \in \mathbb{F}_{n}$ can be decomposed as $k=k_0+k_1\lambda \pmod n$ for $k_i\approx \sqrt{n}$



```python
k = randint(1,n)
k0,k1 = glv_decompose(k,lamb,n)
print(f"k0: {k0}, size: {k0.nbits()} bits")
print(f"k1: {k1}, size: {k1.nbits()} bits")
```

We can consider higher dimensional decompositions: $k=\sum_{i=0}^r k_i\lambda^i \pmod n$
Since $\lambda^2+\lambda+1=0 \pmod n$ then decompositions with $r>2$ alone do not improve anything:



```python
kis = glv_decompose(k,lamb,n,dim = 4)
print("".join(f"k{i} bitsize: {ki.nbits()}\n" for i,ki in enumerate(kis)))
```

Decomposition can be done using other endomorphisms. All of them are of the form $a+b\omega$ where $a,b \in \mathbb{Z}$. "Fast endomorphisms have small degree (typically deg=1 and elements of $O_K^{\times}$). Using $deg(a+b\omega)\geq |a^2-b^2|$ we get:

- deg 1: $a=\pm 1$, $b=0$ (which is $\pm 1$) and $a=0$, $b=\pm 1$ (which is $\pm \omega$) and $a=b=\pm 1$ (which is conjugate to $\omega$). Probably no others?
- deg 2: nothing (call E.isogenies_prime_degree(2) to verify)
- deg 3: $\sigma = \omega-1$ and $-\sigma$. Again, $\sigma$ satisfies quadratic equation: $\sigma^2+3\sigma+3 = 0 \pmod n$

These are not necessarily all of them



```python
delta, sigma_map = find_sigma(E)
print(f"sigma map: {sigma_map}")
print((delta**2+3*delta+3)%n==0)
```

**Idea:** Does it make sense to minimize the decomposition with respect to something else then the size of $k_i$? What about the discrete log with base $\sigma$, i.e. minimize $\log_{\sigma}(k_i)$?



```python
GF(n)(delta).multiplicative_order()
```

Decomposition using more than one endomorphism will not work due to linear dependency of $\sigma$ and $\omega$ (every endomorphism is an element of $\mathbb{Z}[\omega]$)



```python
kis = glv_decompose_two(k,lamb,delta,n)
print("".join(f"k{i} bitsize: {ki.nbits()}\n" for i,ki in enumerate(kis)))
```

**Bitcoin isogenies**




```python
for l in prime_range(20):
    print(f"Number of {l}-degree isogenies: {len(E.isogenies_prime_degree(l))}")
```

Careful: The 2nd Modular polynomial has roots for $j=0$. The isogeny is probably not normalized or whatever and therefore not found by the isogenies_prime_degree method.



```python
ClassicalModularPolynomialDatabase()[2](0,PolynomialRing(F,'j2').gen()).roots()
```

If you call the method on EllipticCurve_from_j(F(54000)) then the codomain has a=0. Composing with isomorphism you can enforce E to be the codomain



```python
print("Isogenies of degree 3:")
for isog in E.isogenies_prime_degree(3):
    print(f"Domain j: {E.j_invariant()}, Codomain j:{isog.codomain().j_invariant()}")
```

The first isogeny, call it $\alpha_3$, is a loop on j-invariants but not an endomorphism of curves (only isogeny). But we can compose it with an isomorphism $\mu$ "back" to bitcoin curve to get endomorphism $\mu\alpha_3 = \sigma\omega$ of degree 3:



```python
isog = E.isogenies_prime_degree(3)[0]
print(f"alpha_3: {isog.rational_maps()}")
u  =F(-3)**(-1)
print(f"mu*alpha_3: {u*isog.rational_maps()[0]}")
print(f"omega*sigma: {beta*sigma_map}")
```

**Idea**: Can we talk about an "isogeny" decomposition? Let $\phi_1,\phi_2,\phi_3:E_1\to E_2$ be thre isogenies and $P\in E_2$ and $kP=Q$ which we wish to compute. Let $P = \phi_1(R)$ for some $R \in E_1$. Then $kP = k\phi_1(R)=a\phi_2(R)+b\phi_3(R)$. If $a,b$ are small this could simplify the computation (probably not small, check the degrees)

#### DCP problem

Motivation: For the ZVP side-channel attack, the attacker guesses some upper bits $k$ of a secret scalar $d$ used for scalar multiplication. If he can find point $P$ (attacker has control over the input point for the SM) such that some computations during the SM involving $kP$ cause zero-value register then this can be seen in the power/EM analysis (assuming the guess of $k$ is correct). We can formalize this as a mathematical problem:

Let $k \in \mathbb{F}_n$ and $f\in \mathbb{F}_p[x_1,x_2]$. Find $P=(x_1,y_1),Q=(x_2,y_2)\in E$ such that $kP=Q$ and $f(P,Q)=0$

Typical examples of $f$: $f(x_1,x_2) = x_1x_2+1$, $f(x_1,x_2) = x_1+x_2$, $f(y_1,y_2) = y_1+y_2$. 

Only known approach: compute multiplication-by-$k$ map $m_k(x_1)$ and substitute: $f(x_1,m_k(x_1))=0$ and find roots of this one-variable polynomial. For $k>2^40$ it is infeasible to compute $m_k$.

Possible improvement can be done by searching for small $l$ ($m_l$ is computable) such that $lk$ is small as well and solve $f(m_l(x_1),m_{lk}(x_1))$. However, the probability (assume uniform) that $lk<2^b$ is $2^{b-256}$. Although we have $2^b$ candidates for $l$, the chance is negligible.

Sources:

[Formula for disaster](https://link.springer.com/content/pdf/10.1007/978-3-030-92062-3_5.pdf)

[ZVP](https://link.springer.com/content/pdf/10.1007/10958513_17.pdf)

**DCP with Bitcoin**

Let $k=k_1+k_2\lambda$ be a decomposition then $m_{k_2\lambda}(x)=m_{k_2}(\beta x)$. Also let $S_3(x_1,x_2,x_3)$ be the third Semaev polynomial (polynomial with the property $A+B+C=\infty$ iff $S_3(A_{x},B_{x},C_{x})=0$). 

We are looking for $x_1,x_2$ such that $S_3(x_2,m_{k_1}(x_1),m_{k_2}(\beta x))$ and $f(x_1,x_2)=0$. If we can express $x_2$ from the second equation and substitute into the first we have again one-variable polynomial. This time $k_1,k_2$ are much smaller then $k$ (half the bit length). However, for $256$-bit curve it is still not enough (128 bit).

In principle, we could use higher dimensional decompositions and higher degree Semaev polynomial (in the same way). The degree of $m$-th Semaev polynomial is $2^{m-2}$ in each variable and $(m-1)2^{m-2}$ total degree. If we have $r$-dim decomposition then we need to compute $\sqrt[r]{n^2}$-degree division polynomial and $f_{r+1}$ Semaev polynomial. In total, the degree after substitution of division polynomial into Semaev would be $r2^{r-1}\sqrt[r]{n^2}$ where $n\approx 2^{256}$. We get $r2^{2\cdot 256/r+r-1}$ which is decreasing for $r<120$. However, we have no such decomposition (of dimension $>2$) and the final degree is too high for every decomposition.



```python
print(semaev(2,a=0,b=7))
print(semaev(3,a=0,b=7))
```

**Idea:** We can modify the trick with small $l$ and $lk$ with decomposition. Instead of requiring $l$ to be small we can require $l_1,l_2$ to be small in $l=l_1+l_2\lambda$. This gives us twice (in bits) the freedom for $l$. But still not enough.

**Idea:** Can we decompose using anything other maps than endomorphisms, possibly general maps? We just need $kP = k_1P+k_2\alpha(P)$ for one point $P$ (if $\alpha$ is a homomorphism then it holds for all $P$ (cyclic group))

#### DCP transport

Let $E:y^2=x^3+b \to E':y^2=x^3+u^6b$ be an isomorphism given by $\mu_u: (x,y) \mapsto (xu^2,yu^3)$

- if $f=x_1+x_2$:
    then if $P,Q$ is a solution to DCP(k) on $E$ then $\mu_u(P), \mu_u(Q)$ is a solution to DCP(k) on $E'$. This follows from $x_1+x_2=0$ $\iff$ $u^2x_1+u^2x_2=0$. Hence if we solve DCP on any curve iso to E then we have a solution on E. 
    
    - *The problem is to solve this anywhere in the isomorphism class*
- if $f=x_1x_2+1$ and $P,Q=kP$ are any points on $E$ such that $-x_1x_2$ is a quadratic residue, i.e. $-x_1x_2=u^2$, then $\mu_{u^{-1/2}}(P)$, $\mu_{u^{-1/2}}(Q)$ is a solution of  DCP(k) on $E': y^2=x^3+u^3b$ (this is possibly a twist depending on whether $u$ is a square). We can therefore find a solution of DCP on some curve in the isomorphism class of $E$ but we have no control on which curve.

    - *The problem is to solve this exactly on curve $E$*
    - *It is easy to find a solution on some curve in the isomorphism class*

- if $f=y_1\pm y_2$ then $x_1^3=x_2^3$ and hence $x_1 = \beta x_2$, $x_1 = \beta^2 x_2$ or $x_1 = x_2$ which means that $k \in \{\pm 1, \pm \lambda, \pm \lambda^2\}$.

    - *The problem is easy but almost always has no solution*





```python
#help(dcp_instance)
E, P, Q, k = dcp_instance(bits = 20)
assert k*P==Q and P[0]*Q[0]+1==0
```

#### $m_k$ transport

Clearly for any isomorphims $\mu: E:y^2=x^3+b \to E':y^2=x^3+u^6b$ we have $\mu(kP)=k\mu(P)$. If we denote $m_k(b,x)$ as multiplication map on curve with coefficient $b$, then $um_k(b,x)=m_k(bu^3,ux)$. In particular $xm_k(b,x) = m_k(bx^3,x^2)$.
- If $x_1+m_k(b,x_1)=0$ which equivalent to $1+m_k(b',1)=0$ for $b'=b/x^3$. *The goal is to find a curve on which the point with $x$-coordinate 1 multiplies-by-k to a points with $x$-coordinate -1*
- If $x_1x_2=-1$ is equivalent to $m_k(bx_1^3,x_1^2)=-1$

#### $\lambda$ transport

$m_{k\lambda}(x)= m_k(\beta x) = \beta m_k(x)$ implies:
- if $x_1+x_2=0$ then $x_1$ is a solution to $x+m_{k_1}(x)=0$, $\beta x+m_{k_2}(x)=0$, $\beta^2x+m_{k_3}(x)=0$ where $k_1=k,k_2=\lambda k, k_3 = \lambda^2 k$
- if $x_1x_2=-1$ then $x_1$ is a solution to $xm_{k_1}(x)=-1$, $xm_{k_2}(x)=-\beta$, $xm_{k_3}(x)=-\beta^2$

Experimentally, gcd between every 2 is $x+x_1$ or a quadratic/cubic polynomial (in the rare case of two/three solutions). **Idea**: use "recursive gcd" using for example $\psi_{m+n}\psi_{m-n}\psi_{r}^2= \psi_{m+r}\psi_{m-r}\psi_{n}^2+\psi_{n+r}\psi_{n-r}\psi_{m}^2$. Another approach is to use Bezout identity: If $e_1$ and $e_2$ are two polynomials with the solution $x_1$ as a common root, consider $u(x)e_1(x)+v(x)e_2(x)=x+x_1$. We can evaluate $e_1,e_2$ at any point, can we do the same for $u,v$? Notice that if $k=0 \pmod 3$ and $b$ is a quadratic residue then $(0,\sqrt{b})$ is a point of order 3 and hence $e_1(0)=0$.



```python
Es, P, Q, k = dcp_instance(p=350.next_prime())
n = Es.order()
p = Es.base_field().order()
print(f"DCP instance for x1*x2+1=0: {Es},P={P},Q={Q},k={k}")
lamb,beta = find_lambda(Es)
k1,k2,k3 = k,(lamb*k)%n,(lamb**2*k)%n
print(f"k1: {k1}, k2: {k2}, k3: {k3}")

x = PolynomialRing(Es.base_field(),'x').gen()
mk1_num = Es._multiple_x_numerator(k1, x)
mk1_den = Es._multiple_x_denominator(k1, x)
mk2_num = Es._multiple_x_numerator(k2, x)
mk2_den = Es._multiple_x_denominator(k2, x)
mk3_num = Es._multiple_x_numerator(k3, x)
mk3_den = Es._multiple_x_denominator(k3, x)

# For f = x_1*x_2+1=0:
eq1 = x*mk1_num+mk1_den
eq2 = x*mk2_num+beta*mk2_den
eq3 = x*mk3_num+beta**2*mk3_den
print(gcd(eq1,eq2).roots(multiplicities=False))
```

**Another viewpoint:**

Is the DCP problem difficult on every curve of given bitlength? The DLP is weak is on the following curves:
- smooth order
- low embedding degree
- supersingular curves (low e.d.)

- not really weak on curves with low CM-discriminant (such as Bitcoin curve)

The DCP and the DLP problem can be thought of as inverse problems: Consider the set $S = \{(P,Q)\mid f(P_x,Q_x)=0\}$. 
- Given a tuple $(P,Q)\in S$ find $k$ such that $kP=Q$ is the DLP (not the general but almost all DLPs have the same difficulty)
- Given a scalar $k$ find a tuple $(P,Q)\in S$ such that $kP=Q$ is the DCP 

#### Space of freedom

Define the following types:
- Multiscalars $M=\{(k_1,\dots,k_m)\mid a_i \in \mathbb{F}_n, m \in \mathbb{N}\}$. This represents a factorization of a scalar. We identify $(k_i)$ with $k_i$.
- $\lambda$-tuples $T_{\lambda} = \{[k_1,k_2]\in \mathbb{F}_n\times\mathbb{F}_n\}$ which represent scalar decompositions $k_1+k_2\lambda$

We also denote $MT_{\lambda}$ a set of rooted trees where each node is of two types: $M$ or $T_{\lambda}$. To each node we additionally assign a scalar represented by the type (factorization, decompozition). The edges are defined as follows:
- if $V=(k_1,\dots,k_m)\in M$, then $V$ has $m$ children with assigned scalars $k_1,\dots$ and $k_m$
- if $V=[k_1,k_2] \in T_{\lambda}$, then $V$ has two children with assigned scalars $k_1$ and $k_2$.

Moreover the root and leafs are multiscalars of length 1.
Example: The scalar 36 can be represented as a $MT_{2}$ tree:

      36(M)
    /   |   \
4(M)  3(T_2)  3(M)
       /  \
     1(M)  1(M)
We consider the following functions on the nodes of trees in $MT_{\lambda}$:
- $\lambda$-decomposition $F_1$: Given a leaf $V\in M$: change the leaf to decomposition node (with two children)
- $\lambda$-division $F_2$: Given a leaf $V \in M$: divide the leaf by $\lambda$
- factorization (not necessarily full) $F_3$: change the leaf to a multiscalar (with children)
- multiplication $F_4$: Given a multiscalar with leafs as children, change the node to a leaf
- $l$-inverse $F_5$: Given a leaf $V$ with scalar $k$, change it to a multiscalar (with children) $(lk,l^{-1})$
- $\lambda$-composition $F_6$: Given a $T_{\lambda}$ node with (two) children as leafs, change the node to a leaf by composing "back"

**Idea:** The goal is to start with a given scalar $k$ and a corresponding one-node tree, apply functions $F_i$ repeatedly with a one-node tree as a result which has small enough scalar

Problem: include the $l$-trick somehow (two trees?)

#### Another pathological cases

The DCP problem for $f=x_1+x_2$ is easy on curves $E:y^2=x^3+ax$ over $\mathbb{F}_p$ for $p=1\pmod 4$. For such curves the map $(x,y)\mapsto(-x,iy)$, for $i=\sqrt{-1}$, is an endomorphism. The characteristic equation is $x^2+1=0 \pmod m$ where $m$ is the order of $(x_1,y_1)$ (found out experimentally). Here we are not considering the group order as the modulus as the curve never has a prime order (Why?). Anyway, if $k$ exists then it must be the root of the equation modulo some divisor of the group order.

For $f=x_1+x_2$ and curves $E:y^2=x^3+ax$ over $\mathbb{F}_p$ with $p=3\pmod 4$, the DCP doesn't have a solution as $y_1^2=-y_2^2$ but $-1$ is not a quadratic residue.

#### DCLP

(assume that the elliptic curve group is cyclic or deal with some minor problems)

Another problem related to DCP problem is the following (DCLP) problem:
- Given $G_1, G_2$, $k$ satisfying $G_2 = kG_1$ and $f$, find scalar $l$ such that $f(lG_1,lG_2)=0$.

The DCP problem reduces to DCLP:
- Given $k$ and $f$, pick any generator $G_1$. Then any $P,Q$ such that $kP=Q$ can be expressed as $P=lG_1$ and $Q=lG_2$ and $f(P,Q)=0$ is equivalent to $f(lG_1,lG_2)=0$.

The DCLP problem reduces to DCP assuming DLP is easy:
- Given $G_1, G_2$, $k$ and $f$, solce DCP for $k$,$f$. For resulting points $P,Q$, solve the DLP for one of the pairs $(Q,G_2)$ or $(P,G_1)$. The solution is the scalar $l$.

The DLP problem reduces to DCLP:
- Given $P,Q$ solve DCLP for $G_1=G_2=P$, $k=1$, $f=x_1-Q_x$. The resulting scalar $l$ is the DL for $P,Q$.


Statements above imply that DCLP and DCP are equivalent if DLP is easy. In particular this is the case of:
- Curves with smooth order
- Curves with small embedding degree
- Anomalous curves

Conversely: If DCP is easy then DCLP and DLP are equivalent. For now, we know only about two cases for which DCP is easy (above):
- If $f=y_1+y_2$ and $E:y^2=x^3+b$
- If $f=x_1+x_2$ and $E:y^2=x^3+ax$
- Both the cases from above are a special case of $f$ representing scalar multiplication (up to sign). More precisely, the DCP is easy if $f=u(x_1)-x_2v(x_1)$ where $u(x)/v(x)$ is the x-coordinate representation of $k$-scalar multiplication (it could be any $l$-scalar multiplication but this would require $Q=kP=lP$).

**Idea**: Does it make sense to talk about equivalence: "DCLP and DCP are equivalent iff.." and if so, does it hold?


#### DC(L)P in a finite field

We can consider the problems in $\mathbb{F}_p$:
- DCP: Given $k$ and $f\in \mathbb{F}_p[x_1,x_2]$, find $g$ such that $f(g,g^k)=0$
- DC(L)P: Given $g_1,g_2=g_1^k$ ($g_1$ being generator) and $f\in \mathbb{F}_p[x_1,x_2]$, find $l$ such taht $f(g_1^l,g_2^l)=0$

Since $f(x_1^l,x_2^l)=f(x_1^l,x_1^{kl})=f'(x_1)$, in DCLP (and similarly in DCP) we are looking for any root of a univariate polynomial of large degree. Complexity of factoring algorithms is polynomial in the degree and $l\approx p $ so brute-forcing through $l$ is still better.

**Idea**: Can we somehow utilize the special form of the polynomial $f'$?

Examples:
- $f=x_1+x_2$ (as for ECs): we want $g_1^{lk}=-g_1^l=g_1^{l+(p-1)/2}$..easy
- $f=x_1+x_2-3$, i.e. we want $g+g^k=3$ (or $g_1^l+g_1^{lk}=3$) for some $g$..hard?

#### Isogenies and DC(L)P

We can further generalize DC(L)P by extending the skalar $k$ to isogenies (including endomorphisms):
- Isogeny DCP: Given a polynomial $f$ and an isogeny $\phi:E_1\to E_2$, find $P\in E_1$, $Q \in E_2$ such that $f(P,Q)=0$
- Isogeny DCLP: Given a polynomial $f$, points $G_1\in E_1, G_2\in E_2$ satisfying $\phi(G_1)=G_2$ where $\phi:E_1 \to E_2$ is an isogeny. Find a scalar $l$ such that $f(lG_1,lG_2)=0$.

#### Old ideas from Vlada:
- new models of equations that consider the curve order
- analogies of DCLP in other groups - finite field arithmetic, binary ECC (Weil descent?), ..., quantum security, analogy of BSGS and Pollard
- what assumptions are necessary for DCLP to be hard? A formal model might have a hash function flavor, could this be used for arithmetic hashes?
- generalizations of DCLP to isogenies/homomorphisms, abelian hidden shift problem
- new DCP approaches: capturing smoothness by periodicity of $m_k$, recurrences modulo $m_r, m_k, m_{k^{-1}}$, formal scalarmult using XZ coords (seems to lead to big Grobner basis computations); leveraging smoothness in some other way - the equation system seems quite non-generic

#### Reductions

- DCP and DLP reduce to DCLP. Moreover, if we consider the ordered set of problems (on a group) $P_1\leq P_2$ whenever $P_1$ reduces to $P_2$, then DCLP is the supremum of DCP and DLP. Natural question: are there any non-polynomial problems that are smaller than both DCP and DLP. Is there an infimum of DCP and DLP?
- the file assumptions.txt contains sources to problems that are related to DLP. One interesting problem is the subsetsum problem in integers and on a curve. 
    - DLP reduces to subsetsum problem on a curve
    - subsetsum problem is NP-complete. In integers there are good heuristics that are not transferable to EC version (if they were DLP would be easier).
    - if DLP is easy then EC-subsetsum problem reduces to the integer version
    - there is also a multi-target version of subsetsum problem that is very close to the DCLP problem

#### Generic group model

- Idea: prove that the DCLP problem is hard (probably O(n)) in the generic group model. The DLP problem is O(sqrt(n)) hard in this model (see [1](https://www.shoup.net/papers/dlbounds1.pdf),[2](https://math.mit.edu/classes/18.783/2017/LectureNotes10.pdf),[3](https://crypto.stanford.edu/cs355/19sp/lec11.pdf)).
- If DCLP is O(n) then so is DCP since DCLP=sup(DLP,DCP) and DLP is O(sqrt(n)).
- If DC(L)P is O(n) then no generic algorithms (of type Pollard, Pohlig-Hellman or Baby step giant step) can be used to solve it.
- The fact that for some polynomials and for some groups the DC(L)P is easy is due to some special properties of the curve and/or the polynomial.

## When is DC(L)P easy?
- intuitive assumption: DC(L)P is difficult since we are looking for two points that satisfy two relations at the same time ($Q=kP$ and $f(P,Q)=0$) and these relations differ qualitatively. 
- goal: make the relation of the same type. Either:
    1. Transform $Q=kP$ into polynomial expressions. This can be done easily in principle since the group law can be expressed using rational functions. However, the degrees of these polynomials are too high.
    2. Transform $f(P,Q)=0$ into some form of "group relation". 

#### Group relations 
Rough definition: We call any binary relation $\sim$ on the set of all points $\{G_1,\dots,G_n\}=E$ of group a group relation if there exist integers $k_1,\dots k_n$ such that for all $P,Q\in E$: $G_j\sim G_k \iff \sum k_iG_i=0$.
This can be simplified: $P\sim Q \iff P+lQ+R=0$ for some fixed $l$ and $R$.
- For $R=0$ we get scalar multiplication
- For $l=1$ we get translation by a point

The so-far discovered easy cases ($f=y_1+y_2$ on $E:y^2=x^3+b$ and $f=x_1+x_2$ on $E:y^2=x^3+ax$) correspond to the fact that $f(P,Q)=0 \iff Q=\lambda P $ for some fixed $\lambda$ (that comes from an action of an endomorphism).

Observations: 
- If $f$ is homogenous under curve isomorphisms: $f(i(P),i(Q))=u\cdot f(P,Q)$ (such as $x_1+x_2$) then $f$ is a group relation either on the whole class of isomorphic curves or nowhere (this comes from DCP transport, see above).

In this model, DCP is easy if $f$ is a group relation.

#### Looking for group relations
- Goal: find an elliptic curve where a given polynomial $f$ can be expressed as a group relation. In other words, find a curve where for <em>all</em> $P,Q$: $f(P,Q)=0 \iff P+lQ+R=0$ for some fixed $l$ and $R$. Unfortunately, there is no clear way how to do this. Even if we have guessed the correct elliptic curve then looking for $l$ seems to be as hard as the DLP. Two simplifications:
    - Assume that $l$ is small (see below).
    - Assume that the curve is supersingular. In that case, the curve is determined up to an isomorphism (we assume curves over Fp) and the DLP can be solved. This might be easy for some cases. For instance, if $f=x_1+x_2$ then we can pick any supersingular curve (see the observation from above). Then we pick any two pairs of points $P_1,Q_1,P_2,Q_2$ satisfying $f$ and we compute $dlog(P_1-P_2,Q_1-Q_2)=l$ then $R=-P-lQ$. This might be relevant for isogeny based systems.
 
#### Small group relations
Assume for simplicity that $f$ is in $x_1,x_2$. Assume $l$ is small and we guess it. Then we can express multiplication-by-$l$ symbolically using $a,b,x_2$ as unknowns and plug it into the [third Semaev polynomial](https://eprint.iacr.org/2004/031.pdf). We get a polynomial $\Phi$ in $x_1,x_2,x_3,a,b$. For some $a,b,x_3$ this polynomial should be zero for all $x_1,x_2$ satisfying $f(x_1,x_2)=0$. In other words, $\Phi$ is zero in $\mathbb{F}_p[x_1,x_2]/(f)$ for some unknown $a,b,x_3$. 

We can reduce $\Phi$ in $\mathbb{F}_p[x_1,x_2,x_3,a,b]/(f)$ and then interpret it as a polynomial in $x_1,x_2$ with coefficients as polynomials in $x_3,a,b$. The fact that the reduced polynomial in $x_1,x_2$ should be zero can be expressed as a system of equations in $x_3,a,b$. Solving these equations is equivalent to finding the curve and the point $R$. 



```python
import relations
import importlib
importlib.reload(relations)
# Load formulas from https://github.com/crocs-muni/formula-for-disaster/tree/main/unrolling/unroll
expressions = relations.load_expressions("unroll/unroll_dadd_shortw.txt")
affine_expressions = relations.simplify_formulae(expressions)
reduced = copy(affine_expressions)
X1,X2,Z1,Z2,Y1,Y2,a,b = list(reduced)[0].parent().gens()
reduced = reduced-set([a,b,X1,X2,Y1,Y2,Y1+Y2,X1-X2]) #remove trivial cases

# Apply the strategy from above
for f in reduced: 
    if f.degree(Y1)==f.degree(Y2)==0: # ignoring polynomials containing y1,y2 for now
        relations.relation_ideal(f,l=1,field=GF(ZZ(1000).next_prime()))
```

#### Findings
- For $f=x_1x_2-a$, any curve with $b=0=x_3$ is solvable. This is relevant, for example, for the SIKE curve.
- For $f=x_1x_2-x_1b-x_2b+3$, any curve with $a=1$ and $x_3=b$ are solutions to $b^3+2b=0$.

### Systematic analysis of EFD addition formulae

Idea: Go through all formulae for addition in EFD database and analyze all instances of DCP
Steps:

    1. Preprocess formulae from EFD (crocs unrolled version), in sort_formulae.ipynb
        - remove denominators
        - convert them to affine form
        - convert formulae from non-standard forms (inverted, extended,..)
        - remove unnecessary constants
    2. Divide formulae into three classes: easy-zpa, hard-zpa, no-zpa (see sort_formulae.ipynb)
        - easy-zpa contains those formulae which contain easy DCP (=depending only on one point)*
        - hard-zpa contains formulae which are not in easy-zpa but have some zpa*
        - no-zpa contains formulae which do not permit any zpa attack*
        - You can use previous results that (or your own) further distinguish easy/hard DCP problems (see sort_formulae.ipynb)
    3. Write up the division of formulae here with some overall summary of interesting things
    4. Go through the DCP problems in easy-zpa classes and classify (to the possible extent) solutions and mitigations
        - even though the DCP problems are easy in general, on some curves the solutions might not exist-> mitigation
        - write up here
    5. Go through the hard problems in hard-zpa classes and analyze them deeper
        - even though some of them are hard, on some curves the solution might be easy, identify those
        - find_relations.py might be useful for those
        - mention isomorphism transformations
        - mentions Sorina's idea
        - write it up here
    6. Based on (4) create:
        - series of tests that will go through standard curves and identify the formulae in which the curves are vulnerable (use DiSSECT)
            - emphasis on the curves that might have been recommended for the particular formulae
        - for each formulae create a curve that can be used for those formulae and is not vulnerable
    7. Based on (5) create:
        - tests that test whether a given curve is one of the special ones for which the DCP is easy (there will probably not be many)
        - create curves that are vulnerable to those zpa attacks
    8. Write up the results on standard curves here
    9. Write up somewhere the curves from (6) and (7)
    10. Write up overall conclusion
    
(*) We assume that rpa attack is not possible on this curve (is somehow mitigated) and so we ignore it. If we assume that RPA attack is possible than this is attacker's easiest path and should ignore zpa attacks.


#### Summary of the results of the analysis 
(details for each category are below)

Note: We assume that rpa attack is not possible (there are no zero-value points in the subgroup used for scalar multiplication) and so we ignore it. If we assume that RPA attack is possible than this is attacker's easiest path and should ignore zpa attacks.

- Edwards addition formulae are all either resistant to ZPA or the underlying DCP problem is, in general, difficult (previous work claimed that all formulae are resistant)
- Weierstrass addition (including differential) formulae are all not resistant to ZPA. However, all the underlying DCP problems are hard (i.e. there are no polynomials in one variable for instance)
- Twisted Edwards addition formulae are all either resistant to ZPA or the underlying DCP problem is, in general, difficult (previous work claimed that all formulae are resistant)
- Edwards differential addition formulae are all vulnerable to easy ZPA attacks
- There is exactly one Montgomery (diff) addition formulae that is ZPA resistant, all the other have ZPA attacks with easy underlying DCP problem
- All Ladder formulae (of all forms) have easy ZPA attacks

#### Edwards addition

**No ZPA:**
inverted:madd-20080225-hwcd, inverted:add-20080225-hwcd, inverted:xmadd-2007-bl, projective:xmadd-2007-hcd, projective:madd-2007-bl-2, projective:add-2007-bl-2

**Hard ZPA:**
inverted:add-2007-bl, inverted:madd-2007-bl, inverted:mmadd-2007-bl: {X2*Y1 + X2*X1 + Y2*X1} 

projective:add-2007-bl, projective:madd-2007-bl, projective:mmadd-2007-bl: {X2*Y1 + Y2*Y1 + Y2*X1} 

projective:madd-2007-bl-3, projective:add-2007-bl-3: {X2*Y1 + Y2*Y1 + Y2*X1, X2*Y2*Y1*d*X1 + 2, 2*X2*Y2*Y1*d*X1 + 1} 

**Easy ZPA:**

nothing

- $X2*Y1 + X2*X1 + Y2*X1$, $X2*Y1 + Y2*Y1 + Y2*X1$, $X2*Y2*Y1*d*X1 + 2$, $2*X2*Y2*Y1*d*X1 + 1$ seem to be hard

#### Short Weierstrass addition

**No ZPA:**
nothing

**Hard ZPA:**
jacobian-3:add-2007-bl, jacobian-3:madd, jacobian-3:add-1998-cmo, jacobian-3:add-1998-cmo-2, modified:add-2009-bl, modified:add-1998-cmo-2, jacobian-0:add-2007-bl, jacobian-0:madd, jacobian-0:add-1998-cmo, jacobian-0:add-1998-cmo-2, projective:madd-1998-cmo, projective:add-1998-cmo, projective:add-1998-cmo-2, xyzz-3:add-2008-s, xyzz-3:madd-2008-s, jacobian:add-2007-bl, jacobian:madd, jacobian:add-1998-cmo, jacobian:add-1998-cmo-2, projective-3:madd-1998-cmo, projective-3:add-1998-cmo, projective-3:add-1998-cmo-2, projective-1:madd-1998-cmo, projective-1:add-1998-cmo, projective-1:add-1998-cmo-2, xyzz:add-2008-s, xyzz:madd-2008-s: {-X2^3 + 3*X2*X1^2 - 2*X1^3 + Y2^2 - 2*Y2*Y1 + Y1^2, Y2 - Y1, -X2^3 + 3*X2^2*X1 - 3*X2*X1^2 + X1^3 + Y2^2 - 2*Y2*Y1 + Y1^2} 

jacobian-3:add-2001-b, jacobian-3:add-1986-cc, jacobian-0:add-2001-b, jacobian-0:add-1986-cc, jacobian:add-2001-b, jacobian:add-1986-cc: {-X2^3 + 3*X2*X1^2 - 2*X1^3 + Y2^2 - 2*Y2*Y1 + Y1^2, Y2 - Y1, X2 + X1} 

jacobian-3:madd-2008-g, jacobian-3:madd-2004-hmv, jacobian-0:madd-2008-g, jacobian-0:madd-2004-hmv, jacobian:madd-2008-g, jacobian:madd-2004-hmv: {-X2^3 + 3*X2*X1^2 - 2*X1^3 + Y2^2 - 2*Y2*Y1 + Y1^2, Y2 - Y1} 

jacobian-3:madd-2007-bl, modified:madd-2009-bl, jacobian-0:madd-2007-bl, jacobian:madd-2007-bl: {-X2^3 + 3*X2^2*X1 - 3*X2*X1^2 + X1^3 + Y2^2 - 2*Y2*Y1 + Y1^2, X2 - X1 + 1, -X2^3 + 3*X2*X1^2 - 2*X1^3 + Y2^2 - 2*Y2*Y1 + Y1^2, X2 - X1 + 2, Y2 - Y1} 

jacobian-3:mmadd-2007-bl, jacobian-0:mmadd-2007-bl, projective:mmadd-1998-cmo, xyzz-3:mmadd-2008-s, jacobian:mmadd-2007-bl, projective-3:mmadd-1998-cmo, projective-1:mmadd-1998-cmo, xyzz:mmadd-2008-s: {-X2^3 + 3*X2*X1^2 - 2*X1^3 + Y2^2 - 2*Y2*Y1 + Y1^2, -Y2 + Y1, -X2^3 + 3*X2^2*X1 - 3*X2*X1^2 + X1^3 + Y2^2 - 2*Y2*Y1 + Y1^2} 

jacobian-3:zadd-2007-m, jacobian-0:zadd-2007-m, jacobian:zadd-2007-m: {-X2^3 + 3*X2*X1^2 - 2*X1^3 + Y2^2 - 2*Y2*Y1 + Y1^2, Y2 - Y1, -X2^2*X1 + 2*X2*X1^2 - X1^3 + Y2^2 - 2*Y2*Y1 + Y1^2} 

modified:mmadd-2009-bl: {-X2 + X1 - 2, -X2^3 + 3*X2^2*X1 - 3*X2*X1^2 + X1^3 + Y2^2 - 2*Y2*Y1 + Y1^2, -X2 + X1 - 1, -X2^3 + 3*X2*X1^2 - 2*X1^3 + Y2^2 - 2*Y2*Y1 + Y1^2, -Y2 + Y1} 

w12-0:add-2010-cln, w12-0:madd-2010-cln: {X2 + X1, X2 + Y2 - Y1 - X1, -X2^3 + 3*X2*X1^2 - 2*X1^3 + Y2^2 - 2*Y2*Y1 + Y1^2, 2*X2 + Y2 - Y1 - 2*X1, Y2 - Y1} 

projective:add-2007-bl, projective-3:add-2007-bl, projective-1:add-2007-bl: {X2^2 + X2*X1 + X1^2 + a, X2 + X1, Y2^2 + 2*Y2*Y1 + Y1^2 + X2 + X1, X2^2 + X2*X1 + X1^2, 2*X2^4 + 4*X2^3*X1 + 6*X2^2*X1^2 + 4*X2*X1^3 + 2*X1^4 - 3*X2*Y2^2 + 4*X2^2*a - 6*X2*Y2*Y1 - 3*X2*Y1^2 - 3*Y2^2*X1 + 4*X2*a*X1 - 6*Y2*Y1*X1 - 3*Y1^2*X1 + 4*a*X1^2 + 2*a^2, Y2^2 + 2*Y2*Y1 + Y1^2 + 2*X2 + 2*X1} 

projective:add-2016-rcb: {Y2 + Y1, X2*Y1 + Y2*X1, 3*X2*X1 + a, -X2*a*X1 + a^2 - 3*X2*b - 3*X1*b, -X2*X1 + a} 

projective:add-2002-bj, projective-3:add-2002-bj, projective-1:add-2002-bj: {X2^2 + X2*X1 + X1^2 + a, 2*X2^4 + 4*X2^3*X1 + 6*X2^2*X1^2 + 4*X2*X1^3 + 2*X1^4 - 3*X2*Y2^2 + 4*X2^2*a - 6*X2*Y2*Y1 - 3*X2*Y1^2 - 3*Y2^2*X1 + 4*X2*a*X1 - 6*Y2*Y1*X1 - 3*Y1^2*X1 + 4*a*X1^2 + 2*a^2, X2^2 + X2*X1 + X1^2, X2 + X1} 

projective-3:add-2016-rcb: {Y2 + Y1, -X2*X1 + X2*b + X1*b - 3, X2*X1 - 1, X2*Y1 + Y2*X1} 

projective-1:add-2002-bj-2: {X2 + X1 - 1, -2*X2^4 - 4*X2^3*X1 - 6*X2^2*X1^2 - 4*X2*X1^3 - 2*X1^4 + 3*X2*Y2^2 + 6*X2*Y2*Y1 + 3*X2*Y1^2 + 3*Y2^2*X1 + 6*Y2*Y1*X1 + 3*Y1^2*X1 + 4*X2^2 + 4*X2*X1 + 4*X1^2 - 2, X2 + X1, X2 + X1 + 1, X2^2 + X2*X1 + X1^2 - 1} 

**Easy ZPA:**
nothing

All of the above seem to be hard, namely:
- $Y1 - Y2$
- $Y1 + Y2$
- $-X1 + X2$
- $-X1 + X2 + 1$
- $-X1 + X2 + 2$
- $X1 + X2$
- $X1 + X2 - 1$
- $X1 + X2 + 1$
- $-X1 + X2 - Y1 + Y2$
- $-2*X1 + 2*X2 - Y1 + Y2$
- $-X1*X2 + a$
- $-X1*X2 + X1*b + X2*b - 3$
- $X1*X2 - 1$
- $3*X1*X2 + a$
- $Y1^2 + 2*Y1*Y2 + Y2^2 + X1 + X2$
- $Y1^2 + 2*Y1*Y2 + Y2^2 + 2*X1 + 2*X2$
- $X2*Y1 + X1*Y2$
- $X1^2 + X1*X2 + X2^2$
- $X1^2 + X1*X2 + X2^2 - 1$
- $X1^2 + X1*X2 + X2^2 + a$
- $-X1*X2*a + a^2 - 3*X1*b - 3*X2*b$
- $-X1^3 + 2*X1^2*X2 - X1*X2^2 + Y1^2 - 2*Y1*Y2 + Y2^2$
- $-2*X1^3 + 3*X1^2*X2 - X2^3 + Y1^2 - 2*Y1*Y2 + Y2^2$
- $X1^3 - 3*X1^2*X2 + 3*X1*X2^2 - X2^3 + Y1^2 - 2*Y1*Y2 + Y2^2$
- $-2*X1^4 - 4*X1^3*X2 - 6*X1^2*X2^2 - 4*X1*X2^3 - 2*X2^4 + 3*X1*Y1^2 + 3*X2*Y1^2 + 6*X1*Y1*Y2 + 6*X2*Y1*Y2 + 3*X1*Y2^2 + 3*X2*Y2^2 + 4*X1^2 + 4*X1*X2 + 4*X2^2 - 2$
- $2*X1^4 + 4*X1^3*X2 + 6*X1^2*X2^2 + 4*X1*X2^3 + 2*X2^4 - 3*X1*Y1^2 - 3*X2*Y1^2 - 6*X1*Y1*Y2 - 6*X2*Y1*Y2 - 3*X1*Y2^2 - 3*X2*Y2^2 + 4*X1^2*a + 4*X1*X2*a + 4*X2^2*a + 2*a^2$

#### Twisted Edwards addition

**No ZPA:**
extended-1:madd-2008-hwcd-4, extended-1:madd-2008-hwcd-3, extended-1:mmadd-2008-hwcd-3, extended-1:add-2008-hwcd-3, extended-1:mmadd-2008-hwcd-4, extended-1:add-2008-hwcd-4, 

**Hard ZPA:**
inverted:madd-2008-bbjlp, inverted:mmadd-2008-bbjlp, inverted:add-2008-bbjlp, {X2*X1 + X1*Y2 + X2*Y1} 

extended:madd-2008-hwcd, extended:mmadd-2008-hwcd, extended:add-2008-hwcd, projective:madd-2008-bbjlp, projective:mmadd-2008-bbjlp, projective:add-2008-bbjlp, extended-1:madd-2008-hwcd, extended-1:mmadd-2008-hwcd, extended-1:add-2008-hwcd, {X1*Y2 + X2*Y1 + Y2*Y1} 

extended:add-2008-hwcd-2, extended:mmadd-2008-hwcd-2, extended:madd-2008-hwcd-2, extended-1:add-2008-hwcd-2, extended-1:mmadd-2008-hwcd-2, extended-1:madd-2008-hwcd-2, {X2*X1 + X1*Y2 - X2*Y1} 

**Easy ZPA:**
nothing

-$X2*X1 + X1*Y2 + X2*Y1$,$X1*Y2 + X2*Y1 + Y2*Y1$, $X2*X1 + X1*Y2 - X2*Y1$ seem to be all hard

#### Edwards diff addition

**No ZPA:**

nothing

**Hard ZPA:**

nothing

**Easy ZPA:**

:dadd-2006-g-2: {Y2^2*r - 1, Y2^2*r + 1, Y3^2*r - 1, Y3^2*r + 1}, hard:set()

:mdadd-2006-g-2: {Y2^2*r - 1, Y2^2*r + 1, Y3^2*r - 1, Y3^2*r + 1}, hard:set()

:dadd-2006-g: {Y2^2*r - 1, Y2^2*r + 1, Y3^2*r - 1, Y3^2*r + 1}, hard:set()

- $Y^2r\pm 1$ having a solution on the curve is equivalent to $-1$ being a square
- Mitigation: Pick prime $p=3 \pmod 4$ 


#### Short Weierstrass diff addition

**No ZPA:**
nothing

**Hard ZPA:**
:mdadd-2002-it, :mdadd-2002-bj-2, :dadd-2002-it, :mdadd-2002-bj, :mdadd-2002-it-3, :dadd-2002-it-3, {-X2*X3 + a, X2 + X3} 

:dadd-2002-it-2, :mdadd-2002-it-4, :mdadd-2002-it-2, :dadd-2002-it-4, {X2*X3 + a, X2 + X3, X2^2*X3 + X2*X3^2 + X2*a + a*X3 + 2*b} 

**Easy ZPA:**
nothing

- all of the above seem to be hard

#### Montgomery diff addition

**No ZPA:**
:dadd-1987-m, 

**Hard ZPA:**
nothing

**Easy ZPA:**
:dadd-1987-m-2: {X2 + 1, X3 - 1, X2 - 1, X3 + 1}, hard:set()

:mdadd-1987-m: {X2 + 1, X3 - 1, X2 - 1, X3 + 1}, hard:set()

:dadd-1987-m-3: {X2 + 1, X3 - 1, X2 - 1, X3 + 1}, hard:set()

- Mitigitation for the above is to forbid points with $x=\pm 1$. This is equivalent to $\frac{a\pm 2}{b}$ not be a square.
- Curve25519, Curve383187, M-221, M-383, M-511  are vulnerable

#### Short Weierstrass ladder

**No ZPA:**
nothing

**Hard ZPA:**

nothing

**Easy ZPA:**
:mladd-2002-bj: {X2^3 + X2*a + b, -X2^2 + a, X2^4 - 2*X2^2*a + a^2 - 8*X2*b, X2^2 + a}, hard:{-X2*X3 + a, X2 + X3}

:mladd-2002-it: {X2^3 + X2*a + b, -X2^2 + a, X2^4 - 2*X2^2*a + a^2 - 8*X2*b, X2^2 + a}, hard:{-X2*X3 + a, X2 + X3}

:ladd-2002-it: {X2^3 + X2*a + b, -X2^2 + a, X2^4 - 2*X2^2*a + a^2 - 8*X2*b, X2^2 + a}, hard:{-X2*X3 + a, X2 + X3}

:mladd-2002-it-5: {X2 + 1, X2^3 + X2*a + b, 2*X2 + 1, X2^4 - 2*X2^2*a + a^2 - 8*X2*b, X2^2 + a, -X2^2 + a}, hard:{X2*X3 + a, X2 + X3, X2^2*X3 + X2*X3^2 + X2*a + a*X3 + 2*b}

:mladd-2002-bj-3: {X2 + 1, X2^3 + X2*a + b, 2*X2 + 1, X2^4 - 2*X2^2*a + a^2 - 8*X2*b, X2^2 + a, -X2^2 + a}, hard:{-X2*X3 + a, X2 + X3}

:mladd-2002-bj-2: {X2 + 1, X2^3 + X2*a + b, 2*X2 + 1, X2^4 - 2*X2^2*a + a^2 - 8*X2*b, X2^2 + a, -X2^2 + a}, hard:{-X2*X3 + a, X2 + X3}

:mladd-2002-it-3: {X2 + 1, X2^3 + X2*a + b, 2*X2 + 1, X2^4 - 2*X2^2*a + a^2 - 8*X2*b, X2^2 + a, -X2^2 + a}, hard:{-X2*X3 + a, X2 + X3}

:ladd-2002-it-4: {X2 + 1, X2^3 + X2*a + b, 2*X2 + 1, X2^4 - 2*X2^2*a + a^2 - 8*X2*b, X2^2 + a, -X2^2 + a}, hard:{X2*X3 + a, X2 + X3, X2^2*X3 + X2*X3^2 + X2*a + a*X3 + 2*b}

:ladd-2002-it-2: {X2^3 + X2*a + b, -X2^2 + a, X2^4 - 2*X2^2*a + a^2 - 8*X2*b, X2^2 + a}, hard:{X2*X3 + a, X2 + X3, X2^2*X3 + X2*X3^2 + X2*a + a*X3 + 2*b}

:mladd-2002-it-2: {X2^3 + X2*a + b, -X2^2 + a, X2^4 - 2*X2^2*a + a^2 - 8*X2*b, X2^2 + a}, hard:{X2*X3 + a, X2 + X3, X2^2*X3 + X2*X3^2 + X2*a + a*X3 + 2*b}

:ladd-2002-it-3: {X2 + 1, X2^3 + X2*a + b, 2*X2 + 1, X2^4 - 2*X2^2*a + a^2 - 8*X2*b, X2^2 + a, -X2^2 + a}, hard:{-X2*X3 + a, X2 + X3}

:mladd-2002-it-4: {X2 + 1, X2^3 + X2*a + b, 2*X2 + 1, X2^4 - 2*X2^2*a + a^2 - 8*X2*b, X2^2 + a, -X2^2 + a}, hard:{X2*X3 + a, X2 + X3, X2^2*X3 + X2*X3^2 + X2*a + a*X3 + 2*b}

- $X^3 + X*a + b$ corresponds to $y=0$, so this is RPA case
- $-X^2 + a$ to have solution:
    - $a$ and $2\sqrt{a^3}+b$ must be squares
- $X^2 + a$ to have solution:
    - $-a$ and $b$ must be squares
- $X + 1$ to have solution:
    - $b-a-1$ must be a square
- $2X+1$ to have solution:
    - $16b-8a-2$ must be a square
- $X^4-2X^2a+a^2-8Xb$ don't know any nice description of the roots
- (hard) $-X2*X3 + a$ seems to be hard
- (hard) $X2*X3 + a$ seems to be hard
- (hard) $X2 + X3$ seems to be hard
- (hard) $X2^2*X3 + X2*X3^2 + X2*a + a*X3 + 2*b$ seems to be hard



#### Montgomery ladder

**No ZPA:**

nothing

**Hard ZPA:**

nothing

**Easy ZPA:**

:ladd-1987-m-2: {X2 - 1, X2^2 + X2*a + 1, X2 + 1, X3 - 1, X3 + 1}, hard:set()

:mladd-1987-m: {X2 - 1, X2^2 + X2*a + 1, X2 + 1, X3 - 1, X3 + 1}, hard:set()

:ladd-1987-m-3: {X2 - 1, X2^2 + X2*a + 1, X2 + 1, X3 - 1, X3 + 1}, hard:set()

:ladd-1987-m: {X2^2 + X2*a + 1, X2 + 1, X2 + a, X2 - 1}, hard:set()

- For $X=\pm 1$ see diff. addition above
- $X^2+aX+1$ implies $Y=0$ and so is a case of RPA

#### Edwards ladder

**No ZPA:**

nothing

**Hard ZPA:**

nothing

**Easy ZPA:**
:ladd-2006-g-2: {Y2^8*r^2 - 2*Y2^4*r^2 + 1, Y2^4*r - 1, Y2^4*r + 1, Y2^8*r^2 - 2*Y2^4 + 1, Y3^4*r - 1, Y3^4*r + 1}, hard:set()

:mladd-2006-g-2: {Y2^8*r^2 - 2*Y2^4*r^2 + 1, Y2^4*r - 1, Y2^4*r + 1, Y2^8*r^2 - 2*Y2^4 + 1, Y3^4*r - 1, Y3^4*r + 1}, hard:set()

:ladd-2006-g: {Y2^8*r^2 - 2*Y2^4*r^2 + 1, Y2^4*r - 1, Y2^4*r + 1, Y2^8*r^2 - 2*Y2^4 + 1, Y3^4*r - 1, Y3^4*r + 1}, hard:set()
- $Y^8r^2-2Y^4r^2+1$ to have roots:
    - $1-1/r^2$ must be a square
    - one of $1\pm \sqrt{1-1/r^2}$ must be a fourth power, say $y^4$
    - $\frac{1-y^2}{1-r^2y^2}$ must be a square
- $Y^4r\pm 1$ to have roots:
    - $\pm 1/r$ must be a fourth power, say $y^4$
    - one of $1\pm \sqrt{1+y^4}$ must be a fourth power
- $Y^8r^2-2Y^4+1$ to have roots:
    - $1-r^2$ must be a square
    - one of $(1\pm \sqrt{1-r^2})/r^2$ must be a fourth power, say $y^4$
    - $\frac{1-y^2}{1-r^2y^2}$ must be a square



```python

```
